<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    require_once("../../lib/PHPExcel/PHPExcel.php");
    require_once("../model/LectorExcel.php");

    $lector = new LectorExcel();
    $lector->readExcel2017("../../data/reporte.xlsx");
    $lector->objPHPExcel->getActiveSheet()->getStyle('C23:H23')->getNumberFormat()->applyFromArray( 
        array( 
            'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
        )
    );
    // Se crea una copia de la hoja de excel y se le da un nombre
    $tempSheet = $lector->objPHPExcel->setActiveSheetIndexByName("Resumen")->copy();
    $tempSheet ->setTitle("resumen grafico");
    $lector->objPHPExcel->addSheet($tempSheet);
    unset($tempSheet);
    $lector->objPHPExcel->setIndexByName("resumen grafico",0);
    $cells = $lector->objPHPExcel->setActiveSheetIndexByName("resumen grafico")->rangeToArray('A11:L29',NULL,FALSE,FALSE,TRUE);
    $row_range = array_keys($cells);
    foreach ($row_range as $row) {
        $lector->objPHPExcel->setActiveSheetIndexByName("resumen grafico")->removeRow($row); // Aqui se le eliminan las celdas ocupadas para dar espacio a la graficas
    }
    // Se definen las celdas que contendran los datos de las graficas de excel
    $labels = array(
        'Resumen!$D$11',
        'Resumen!$E$11', // Este corresponde a las etiquetadas de datos
        'Resumen!$G$11'
    );
    $values = array(
        'Resumen!$D$13:$D$18',
        'Resumen!$E$13:$E$18', //Esta son los valores
        'Resumen!$G$13:$G$18'
    );
    $xaxisTickValues = array(
        'Resumen!$B$13:$B$18' // Estas son las etiquedas de los datos del eje
    );



    $lector->radial_graph($labels,$xaxisTickValues,$values,6,'B11','K35',"resumen grafico","Alumnos en Módulos de Especialización");
    $xaxisTickValues = array();
    $labels = array(
        'Resumen!$G$22',
        'Resumen!$H$22'
    );
    $values = array(
        'Resumen!$G$23',
        'Resumen!$H$23'
    );


    $lector->stacked_bar_graph($labels,$xaxisTickValues,$values,1,'B36','K50',"resumen grafico","Eficiencia Terminal y de Titulación");
    
    $xaxisTickValues = array();
    $labels = array(
        'Resumen!$C$22',
        'Resumen!$D$22',
        'Resumen!$E$22',
        'Resumen!$F$22',
        'Resumen!$G$22',
        'Resumen!$H$22'
    );
    $values = array(
        'Resumen!$C$23',
        'Resumen!$D$23',
        'Resumen!$E$23',
        'Resumen!$F$23',
        'Resumen!$G$23',
        'Resumen!$H$23'
    );
    $lector->standar_bar_graph($labels,$xaxisTickValues,$values,1,'B56','K80',"resumen grafico","Indicadores de la generación");
    $labels = array(
        'Resumen!$D$11'
    );
    $values = array(
        'Resumen!$D$12:$D$18',
    );
    $xaxisTickValues = array(
        'Resumen!$B$12:$B$18'
    );
    $lector->donut_pie_graph($labels,$xaxisTickValues,$values,7,'B82','K106',"resumen grafico","Matricula actual");
    $labels = array(
        'Resumen!$C$26'
    );
    $values = array(
        'Resumen!$H$27:$H$29',
    );
    $xaxisTickValues = array(
        'Resumen!$C$27:$C$29'
    );
    $lector->donut_pie_graph($labels,$xaxisTickValues,$values,3,'B109','K133',"resumen grafico","Causas de egreso");
    $labels = array(
        '\'Avance Generacional\'!$D$11:$N$11',
        '\'Avance Generacional\'!$O$11',
    );
    $values = array(
        '\'Avance Generacional\'!$Q$12:$Q$31',
        '\'Avance Generacional\'!$O$12:$O$31'
    );
    $xaxisTickValues = array(
        '\'Avance Generacional\'!$C$12:$C$31'
    );
    $lector->create_hidden_sum('D12:N31',array('Q','12'),'row',"Avance Generacional");
    $lector->standar_line_graph($labels,$xaxisTickValues,$values,19,'C34','P61',"Avance Generacional","Avance Generacional");
    $lector->create_plan_grafico_sheet_template("Plan 1425");
    $lector->create_plan_grafico_sheet_template("Plan 1422");
    $lector->objPHPExcel->setActiveSheetIndexByName("resumen grafico");
 
   $lector->print_excel("prueba.xlsx");