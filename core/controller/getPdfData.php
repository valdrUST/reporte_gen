<div class="titulo-seccion">
  <div class="row">
    <div class="col-8">
    <?php   error_reporting(E_ALL); 
    ini_set("display_errors", 1);
    date_default_timezone_set('UTC'); ?>
    <?php   require_once(dirname(__FILE__)."/../model/ProcesadorPDF.php") ?>
    <?php   require_once(dirname(__FILE__)."/../model/Herramientas.php") ?>
    <?php   require_once(dirname(__FILE__)."/../herramientas/AppHerramientas.php") ;
            require_once(dirname(__FILE__)."/../../lib/PHPGraphLib/phpgraphlib.php");
            require_once(dirname(__FILE__)."/../../lib/PHPGraphLib/phpgraphlib_stacked.php");
            require_once(dirname(__FILE__)."/../../lib/PHPGraphLib/phpgraphlib_pie.php");
            require_once(dirname(__FILE__)."/../../lib/PHPPhantomJS/ClientInterface.php");
            require_once(dirname(__FILE__)."/../../lib/PHPPhantomJS/Client.php");

    ?>
    <?php
      $data = array();
      $opt = 1;
      switch($opt){
        case 1:
          $datos = json_decode(file_get_contents(dirname(__FILE__)."/../../data/110_2010_20131.json"));
          break;
        case 2:
          $datos = json_decode(file_get_contents(dirname(__FILE__)."../../data/107_2016_20201.json"));
          break;        
        case 3:
          $datos = json_decode(file_get_contents(dirname(__FILE__)."../../data/137_2020_20201.json"));
          break;
      }
    ?>
      Carrera: <?= $datos->resumenPlanesGeneracion[0]->id_carrera ?> Plan FI: <?= $datos->resumenPlanesGeneracion[0]->id_plan_fi ?>
      Generación: <?= AppHerramientas::formatearSemestreLectivo($datos->resumenPlanesGeneracion[0]->ingreso_fi) ?>

    </div>
    <div class="col-4 text-right">
    </div>
  </div>
</div>

<table class="table tabla-reporte">
  <tr>
    <th>Módulo</th>
    <th>Ingreso Generación</th>
    <th>Matricula Actual</th>
    <th>Egresados</th>
    <th>Egresados en Reglamentario</th>
    <th>Titulados</th>
    <th>Sin Egresar</th>
    <th>Rezagados</th>
    <th>Abandonos</th>
    <th>Desertores</th>
    <th>Inscritos</th>
  </tr>
    <?php foreach ($datos->planesEstudio as $planEstudio): ?>
      <tr>
        <td class="td-text"><?= $planEstudio->planDGAE->id . ' - ' . $planEstudio->planDGAE->nombre ?></td>
          <?php foreach ($datos->resumenPlanesGeneracion as $resumenModulo): ?>
              <?php if ($resumenModulo->id_plan_estudios == $planEstudio->id): ?>
              <td><?= $resumenModulo->ingreso_generacion ?></td>
              <td><?= $resumenModulo->matricula_actual ?></td>
              <td><?= $resumenModulo->egresados ?></td>
              <td><?= $resumenModulo->egresados_reglamentario ?></td>
              <td><?= $resumenModulo->titulados ?></td>
              <td><?= $resumenModulo->sin_egresar ?></td>
              <td><?= $resumenModulo->rezagados ?></td>
              <td><?= $resumenModulo->abandonos ?></td>
              <td><?= $resumenModulo->desertores ?></td>
              <td><?= $resumenModulo->inscritos ?></td>
              <?php endif; ?>
          <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
   
    <?php if (count($datos->planesEstudio) > 1): ?>
      <tr>
        <td class="td-text"><b>Total</b></td>
        <td><b><?= $datos->resumenGeneracion->t_ingreso_generacion ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_matricula_actual ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_egresados ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_egresados_reglamentario ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_titulados ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_sin_egresar ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_rezagados ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_abandonos ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_desertores ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_inscritos ?></b></td>
        <?php
          // Aqui se obtiene los datos de la consulta para la grafica
          array_push($data,$datos->resumenGeneracion->t_ingreso_generacion);
          array_push($data,$datos->resumenGeneracion->t_matricula_actual);
          array_push($data,$datos->resumenGeneracion->t_egresados);
          array_push($data,$datos->resumenGeneracion->t_egresados_reglamentario);
          array_push($data,$datos->resumenGeneracion->t_titulados);
          array_push($data,$datos->resumenGeneracion->t_sin_egresar);
        ?>
      </tr>
    <?php endif; ?>
</table>
<form action="getPdfFile.php" method="post" autocomplete="off" enabled="on">
  <input type="hidden" name="gen" value="true">
  <button type="submit" class="btn btn-info form-group">
    Descargar PDF
  </button>
</form>
<form action="getPdfFile.php" method="post" autocomplete="off" enabled="on">
  <input type="hidden" name="gen_flask" value="true">
  <button type="submit" class="btn btn-info form-group">
    Descargar PDF de flask
  </button>
</form>
      <?php
      $graph = new PHPGraphLib(495, 280,dirname(__FILE__)."/img.png"); //En esta parte define el tamaño de la grafica y su ruta para ser guardada
      $graph->addData($data);
      $graph->setupYAxis("15");
      $graph->setGradient('teal', '#0000FF');
      $graph->setXValuesHorizontal(true);
      $graph->setXAxisTextColor ('navy');
      $graph->setLegend(true);
      $graph->setLegendTitle('M1', 'M2', 'M3', 'M4','M5','M6');
      $graph->createGraph(); // Se crea y se visualiza o se guarda la grafica
      ?>