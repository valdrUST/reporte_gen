<?php      
    error_reporting(E_ALL); 
    ini_set("display_errors", 1);
    date_default_timezone_set('UTC');
    require_once(dirname(__FILE__)."/../../lib/TCPDF/TCPDF.php");
    if(isset($_POST["gen"])){
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // add a page
        $pdf->AddPage();
        
        $pdf->Image(dirname(__FILE__)."/img.png", 15, 140, 120, 113, 'PNG', NULL, '', true, 150, '', false, false, 1, false, false, false); // Se añade la imagen de la grafica al pdf
        ob_clean();
        $pdf->Output('example_009.pdf', 'I');
    }
?>