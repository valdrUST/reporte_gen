<div class="titulo-seccion">
  <div class="row">
    <div class="col-8">
    <?php// error_reporting(E_ALL)  ?>
    <?php// ini_set("display_errors", 1) ?>
    <?php  require_once("../model/Herramientas.php") ?>
    <?php  require_once("../herramientas/AppHerramientas.php") ?>
    <?php
            $opt = 1;
            switch($opt){
              case 1:
                $datos = json_decode(file_get_contents("../../data/110_2010_20131.json"));
                break;
              case 2:
                $datos = json_decode(file_get_contents("../../data/107_2016_20201.json"));
                break;        
              case 3:
                $datos = json_decode(file_get_contents("../../data/137_2020_20201.json"));
                break;
            }
    ?>
      Carrera: <?= $datos->resumenPlanesGeneracion[0]->id_carrera ?> Plan FI: <?= $datos->resumenPlanesGeneracion[0]->id_plan_fi ?>
      Generación: <?= AppHerramientas::formatearSemestreLectivo($datos->resumenPlanesGeneracion[0]->ingreso_fi) ?>
      <script src="../../assets/vendor/js/jquery/jquery.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
      <script>
      var data = {};
      var url;
      opt = 1;
      switch(opt){
        case 1:
          url = "../../data/110_2010_20131.json";
          break;
        case 2:
          url = "../../data/107_2016_20201.json";
          break;        
        case 3:
          url = "../../data/137_2020_20201.json";
          break;
        default:
          break;
      }
      $.ajax({
        async: false,
        url: url, //the path of the file is replaced by File.json
        dataType: "json",
        success: function (response) {
          data = response;
      }
    });
    </script>
    </div>
    <div class="col-4 text-right">
      <form action="reporteGeneracional/accion/generaReporteGeneracional" target="_self"
            method="post" autocomplete="off" enabled="on">
        <input type="hidden" name="crrra" value="<?= $datos->resumenPlanesGeneracion[0]->id_carrera ?>">
        <input type="hidden" name="pln_fi" value="<?= $datos->resumenPlanesGeneracion[0]->id_plan_fi ?>">
        <input type="hidden" name="gnrcn" value="<?= $datos->resumenPlanesGeneracion[0]->ingreso_fi ?>">
        <input type="hidden" name="fchclcl" value="<?= $datos->resumenPlanesGeneracion[0]->fecha_calculo ?>">
        <input type="hidden" name="dscrg" value="s">
        <button type="submit" class="btn btn-info form-group">
          Descargar Reporte
        </button>
      </form>
    </div>
  </div>
</div>

<table class="table tabla-reporte">
  <tr>
    <th>Módulo</th>
    <th>Ingreso Generación</th>
    <th>Matricula Actual</th>
    <th>Egresados</th>
    <th>Egresados en Reglamentario</th>
    <th>Titulados</th>
    <th>Sin Egresar</th>
    <th>Rezagados</th>
    <th>Abandonos</th>
    <th>Desertores</th>
    <th>Inscritos</th>
  </tr>
    <?php foreach ($datos->planesEstudio as $planEstudio): ?>
      <tr>
        <td class="td-text"><?= $planEstudio->planDGAE->id . ' - ' . $planEstudio->planDGAE->nombre ?></td>
          <?php foreach ($datos->resumenPlanesGeneracion as $resumenModulo): ?>
              <?php if ($resumenModulo->id_plan_estudios == $planEstudio->id): ?>
              <td><?= $resumenModulo->ingreso_generacion ?></td>
              <td><?= $resumenModulo->matricula_actual ?></td>
              <td><?= $resumenModulo->egresados ?></td>
              <td><?= $resumenModulo->egresados_reglamentario ?></td>
              <td><?= $resumenModulo->titulados ?></td>
              <td><?= $resumenModulo->sin_egresar ?></td>
              <td><?= $resumenModulo->rezagados ?></td>
              <td><?= $resumenModulo->abandonos ?></td>
              <td><?= $resumenModulo->desertores ?></td>
              <td><?= $resumenModulo->inscritos ?></td>
              <?php endif; ?>
          <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
   
    <?php if (count($datos->planesEstudio) > 1): ?>
      <tr>
        <td class="td-text"><b>Total</b></td>
        <td><b><?= $datos->resumenGeneracion->t_ingreso_generacion ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_matricula_actual ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_egresados ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_egresados_reglamentario ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_titulados ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_sin_egresar ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_rezagados ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_abandonos ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_desertores ?></b></td>
        <td><b><?= $datos->resumenGeneracion->t_inscritos ?></b></td>
      </tr>
    <?php endif; ?>
</table>
<div id="chart_resumen"></div>
    <script> 
      resumenPlanesGeneracion = data.resumenPlanesGeneracion;
      planesEstudio = data.planesEstudio;
        series = [{
          name:'Matricula_actual',
          data:[],
        },{
          name:'Egresados',
          data:[],
        },{
          name:'Titulados',
          data:[],
        }];
        categories = [];

        planesEstudio.forEach(function(planEstudio){
          if(planesEstudio.length > 1){
          if(planEstudio.planDGAE.nombre != "TRONCO COMUN"){
            categories.push(planEstudio.planDGAE.nombre);
            id = planEstudio.id;
            res = resumenPlanesGeneracion.find(x => x.id_plan_estudios == id);
            if(typeof res != 'undefined'){
              series[0].data.push(res.matricula_actual);
              series[1].data.push(res.egresados);
              series[2].data.push(res.titulados);  
            }else{
              series[0].data.push(0);
              series[1].data.push(0);
              series[2].data.push(0);
            }
          }
          }else{
            categories.push(planEstudio.planDGAE.nombre);
            id = planEstudio.id;

            res = resumenPlanesGeneracion.find(x => x.id_plan_estudios == id);
            if(typeof res != 'undefined'){
              series[0].data.push(res.matricula_actual);
              series[1].data.push(res.egresados);
              series[2].data.push(res.titulados);  
            }else{
              series[0].data.push(0);
              series[1].data.push(0);
              series[2].data.push(0);
            }
          }
        });
        var options_modulo = {
          series: series,
          title: {
            text: 'Alumnos en Módulos de Especialización'
          },
          xaxis: {
            categories : categories
          },
          chart: {
          type: 'radar'
          }
        }
        var chart = new ApexCharts(document.querySelector("#chart_resumen"), options_modulo);
        chart.render();
    </script>
<table class="table tabla-reporte">
  <tr>
    <th>Rezago %</th>
    <th>Retención %</th>
    <th>Abandono %</th>
    <th>Deserción %</th>
    <th>Eficiencia Terminal</th>
    <th>Eficiencia de Titulación</th>
  </tr>
  <tr>
    <td><?= $datos->resumenGeneracion->rezago ?> %</td>
    <td><?= $datos->resumenGeneracion->retencion ?> %</td>
    <td><?= $datos->resumenGeneracion->abandono ?> %</td>
    <td><?= $datos->resumenGeneracion->desercion ?> %</td>
    <td><?= $datos->resumenGeneracion->eficiencia_terminal ?> %</td>
    <td><?= $datos->resumenGeneracion->eficiencia_titulacion ?> %</td>
  </tr>
</table>
<div id="chart_eficiencia"></div>
<script> 
      resumenGeneracion = data.resumenGeneracion;
        series = [{
          name: "eficiencia terminal",
          data:[resumenGeneracion.eficiencia_terminal],
        },{
          name: "eficiencia titulacion",
          data:[resumenGeneracion.eficiencia_titulacion],
        }];
        categories = ["eficiencia_terminal","eficiencia_titulacion"];
        var options_eficiencia = {
          series: series,
          title: {
            text: 'Alumnos en Módulos de Especialización'
          },
          xaxis: {
            categories : categories
          },
          chart: {
          type: 'bar'
          },
        plotOptions: {
          bar: {
            horizontal: true,
            dataLabels: {
              position: 'top',
              }
            }
          },
          dataLabels: {
          enabled: true,
          textAnchor: 'start',
          formatter: function (val, opt) {
            return val + "%"
          },
          offsetX: 0,
          dropShadow: {
            enabled: true
          }
        }
        }
        var chart = new ApexCharts(document.querySelector("#chart_eficiencia"), options_eficiencia);
        chart.render();
    </script>
    <div id="chart_indicadores"></div>
    <script> 
      resumenGeneracion = data.resumenGeneracion;
        series = [{
          name: "Eficiencia terminal",
          data:[resumenGeneracion.eficiencia_terminal],
        },{
          name: "Eficiencia titulacion",
          data:[resumenGeneracion.eficiencia_titulacion],
        },
        {
          name: "Rezago %",
          data:[resumenGeneracion.rezago],
        },
        {
          name: "Retencion %",
          data:[resumenGeneracion.retencion],
        },
        {
          name: "Abandono %",
          data:[resumenGeneracion.abandono],
        },
        {
          name: "Desercion %",
          data:[resumenGeneracion.desercion],
        }];
        categories = ["Indicadores de la generacion"];//"Eficiencia terminal","Eficiencia titulacion", "Rezago %","Retencion %","Abandono %","Desercion %"];
        var options_indicadores = {
          series: series,
          title: {
            text: 'Alumnos en Módulos de Especialización'
          },
          legend: {
            show: true,
            showForSingleSeries: true
          },
          xaxis: {
            categories : categories
          },
          chart: {
          type: 'bar'
          },
        plotOptions: {
          bar: {
            horizontal: false,
            dataLabels: {
              position: 'top',
              }
            }
          },
          dataLabels: {
            enabled: true,
            textAnchor: 'start',
            formatter: function (val, opt) {
              return val + "%"
            },
            offsetX: 0,
            dropShadow: {
              enabled: true
            }
          }
        }
        var chart = new ApexCharts(document.querySelector("#chart_indicadores"), options_indicadores);
        chart.render();
    </script>
<table class="table tabla-reporte">
    <?php if (isset($datos->resumenCambios)): ?>
      <tr>
        <th>Causa de Egreso</th>
        <th>Alumnos</th>
      </tr>
        <?php foreach ($datos->resumenCambios as $cambio): ?>
        <tr>
          <td class="td-text"><?= $cambio->id_causa_egreso . ' - ' . $cambio->nombre ?></td>
          <td><?= $cambio->alumnos ?></td>
        </tr>
        <?php endforeach; ?>
    <?php endif; ?>
</table>
<div id="chart_matricula_actual"></div>
<br>
<div id="chart_causas_egreso"></div>
<script> 
      resumenPlanesGeneracion = data.resumenPlanesGeneracion;
      planesEstudio = data.planesEstudio;
        series = [];
        labels=[];
        if(planesEstudio.length > 1){
        planesEstudio.forEach(function(planEstudio){
            labels.push(planEstudio.planDGAE.nombre);
            id = planEstudio.id;
            res = resumenPlanesGeneracion.find(x => x.id_plan_estudios == id);
            if(typeof res != 'undefined'){
              series.push(Number(res.matricula_actual)); 
            }else{
              series.push(0);
            }
        });
        var options_matricula_actual = {
          series: series,
          title: {
            text: 'Matricula Actual'
          },
          legend: {
            position: 'bottom'
          },
          labels:labels,
          chart: {
          type: 'donut'
          },
          plotOptions: {
          pie: {
            donut: {
              labels: {
                show: true,
                total: {
                  showAlways: true,
                  show: true
                }
              }
            }
          }
        },
          dataLabels: {
            enabled: true,
            textAnchor: 'start',
            formatter: function (val, opt) {
              return val.toFixed(2) + "%"
            }
          }
        };
        var chart_1 = new ApexCharts(document.querySelector("#chart_matricula_actual"), options_matricula_actual);
        chart_1.render();
      }
    </script>

<script> 
      resumenCambios = data.resumenCambios;
        series = [];
        labels=[];
        resumenCambios.forEach(function(resumenCambio){
          series.push(Number(resumenCambio.alumnos));
          labels.push(resumenCambio.nombre);
        });
        var options_causas_egreso = {
          series: series,
          title: {
            text: 'Causas de egreso'
          },
          legend: {
            position: 'bottom'
          },
          labels:labels,
          chart: {
          type: 'donut'
          },
          plotOptions: {
          pie: {
            donut: {
              labels: {
                show: true,
                total: {
                  showAlways: true,
                  show: true
                }
              }
            }
          }
        },
          dataLabels: {
            enabled: true,
            textAnchor: 'start',
            formatter: function (val, opt) {
              return val.toFixed(2) + "%"
            }
          }
        };
        var chart_2 = new ApexCharts(document.querySelector("#chart_causas_egreso"), options_causas_egreso);
        chart_2.render();
    </script>
<table class="table small tabla-reporte">
    <tr>
        <th rowspan="2">Semestre</th>
        <th colspan="13">Avance</th>
    </tr>
    <tr>
        <th>0%</th>
        <th>10%</th>
        <th>20%</th>
        <th>30%</th>
        <th>40%</th>
        <th>50%</th>
        <th>60%</th>
        <th>70%</th>
        <th>80%</th>
        <th>90%</th>
        <th>99%</th>
        <th>100%</th>
        <th>Total</th>
    </tr>
    <?php foreach ($datos->avanceGeneracional as $avance): ?>
        <tr>
            <td><?= AppHerramientas::formatearSemestreLectivo($avance->semestre) ?></td>
            <td><?= $avance->a0 ?></td>
            <td><?= $avance->a10 ?></td>
            <td><?= $avance->a20 ?></td>
            <td><?= $avance->a30 ?></td>
            <td><?= $avance->a40 ?></td>
            <td><?= $avance->a50 ?></td>
            <td><?= $avance->a60 ?></td>
            <td><?= $avance->a70 ?></td>
            <td><?= $avance->a80 ?></td>
            <td><?= $avance->a90 ?></td>
            <td><?= $avance->a99 ?></td>
            <td><?= $avance->a100 ?></td>
            <td><?= $avance->total ?></td>
        </tr>
    <?php endforeach; ?>
</table>
<div id="chart_avance_generacion"></div>

<script> 
      avanceGeneracional = data.avanceGeneracional;
        series = [{name:"0-99%",
                  data:[]},
                  {name:"100%",
                  data:[]}];
        labels=[];
        categories = [];
        avanceGeneracional.forEach(function(avance){
          series[0].data.push(Number(avance.a0)+
          Number(avance.a10)+
          Number(avance.a20)+
          Number(avance.a30)+
          Number(avance.a40)+
          Number(avance.a50)+
          Number(avance.a60)+
          Number(avance.a70)+
          Number(avance.a80)+
          Number(avance.a90)+
          Number(avance.a99));
          series[1].data.push(Number(avance.a100));
          categories.push(avance.semestre);
        });
        var options_avance_generacion = {
          series: series,
          title: {
            text: 'Avance Generacional'
          },
          legend: {
            position: 'bottom'
          },
          labels:labels,
          xaxis: {
            categories : categories
          },
          chart: {
          type: 'line'
          },
          plotOptions: {
          pie: {
            donut: {
              labels: {
                show: true,
                total: {
                  showAlways: true,
                  show: true
                }
              }
            }
          }
        },
          dataLabels: {
            enabled: true,
            textAnchor: 'start',
            formatter: function (val, opt) {
              return val
            }
          }
        };
        var chart_3 = new ApexCharts(document.querySelector("#chart_avance_generacion"), options_avance_generacion);
        chart_3.render();
        var dataURL = chart_3.dataURI().then((uri) => {
        console.log(uri);
      });
    </script>
<?php foreach ($datos->planesEstudio as $planEstudio): ?>
  <table class="table tabla-reporte">
    <tr>
      <th colspan="17"><?= $planEstudio->planDGAE->nombre ?></th>
    </tr>
      <?php foreach ($planEstudio->listaSemestre as $semestre => $registroSemestre): ?>
        <tr>
          <th colspan="17" class="th-1">Semestre <?= $semestre ?></th>
        </tr>
        <tr>
          <th class="th-2">Asignatura</th>
          <th class="th-2">Alumnos</th>
          <th class="th-2">Calificaciones</th>
          <th class="th-2">05</th>
          <th class="th-2">06</th>
          <th class="th-2">07</th>
          <th class="th-2">08</th>
          <th class="th-2">09</th>
          <th class="th-2">10</th>
          <th class="th-2">NP</th>
          <th class="th-2">S</th>
          <th class="th-2">B</th>
          <th class="th-2">MB</th>
          <th class="th-2">AC</th>
          <th class="th-2">CO</th>
          <th class="th-2">NA</th>
          <th class="th-2">RE</th>
        </tr>
          <?php foreach ($registroSemestre->listaAsignatura as $asignatura): ?>
              <?php if (is_numeric($asignatura->id)): ?>
            <tr>
                <?php if (isset($asignatura->indices)): ?>
                  <td rowspan="2" class="td-text td-2"><?= $asignatura->id . ' - ' . $asignatura->nombre ?></td>
                  <td rowspan="2" class="td-2"><?= $asignatura->indices->alumnos_considerados ?></td>
                  <td rowspan="2" class="td-2"><?= $asignatura->indices->calificaciones ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_05 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_06 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_07 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_08 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_09 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_10 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_np ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_s ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_b ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_mb ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_ac ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_co ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_na ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_re ?></td>
                </tr>
                <tr>
                  <td class="td-2"><?= $asignatura->indices->i_05 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_06 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_07 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_08 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_09 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_10 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_np ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_s ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_b ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_mb ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_ac ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_co ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_na ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_re ?>%</td>
                <?php else: ?>
                  <td class="td-text td-2"><?= $asignatura->id . ' - ' . $asignatura->nombre ?></td>
                  <td colspan="16" class="td-2">SIN CALIFICACIONES</td>
                <?php endif; ?>
            </tr>
              <?php else: ?>
            <tr>
              <td colspan="17" class="td-2">
                  <?php
                  $ops = explode(',', $asignatura->id);
                  $lbs = array();
                  foreach ($ops as $op) {
                    array_push($lbs, $datos->optativas->$op);
                  }
                  echo '<b>' . implode('</b> u <b>', $lbs) . '</b>';
                  ?>
              </td>
            </tr>
              <?php endif; ?>

          <?php endforeach; ?>
      <?php endforeach; ?>
      <?php foreach ($planEstudio->listaPool as $tipo => $registroTipo): ?>
          <?php if (count($registroTipo->listaAsignatura) > 0): ?>
          <tr>
            <th colspan="17" class="th-1"><?= $datos->optativas->$tipo ?></th>
          </tr>
          <tr>
            <th class="th-2">Asignatura</th>
            <th class="th-2">Alumnos</th>
            <th class="th-2">Calificaciones</th>
            <th class="th-2">05</th>
            <th class="th-2">06</th>
            <th class="th-2">07</th>
            <th class="th-2">08</th>
            <th class="th-2">09</th>
            <th class="th-2">10</th>
            <th class="th-2">NP</th>
            <th class="th-2">S</th>
            <th class="th-2">B</th>
            <th class="th-2">MB</th>
            <th class="th-2">AC</th>
            <th class="th-2">CO</th>
            <th class="th-2">NA</th>
            <th class="th-2">RE</th>
          </tr>
              <?php foreach ($registroTipo->listaAsignatura as $asignatura): ?>
            <tr>
                <?php if (isset($asignatura->indices)): ?>
                  <td rowspan="2" class="td-text td-2"><?= $asignatura->id . ' - ' . $asignatura->nombre ?></td>
                  <td rowspan="2" class="td-2"><?= $asignatura->indices->alumnos_considerados ?></td>
                  <td rowspan="2" class="td-2"><?= $asignatura->indices->calificaciones ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_05 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_06 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_07 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_08 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_09 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_10 ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_np ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_s ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_b ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_mb ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_ac ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_co ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_na ?></td>
                  <td class="td-2"><?= $asignatura->indices->c_re ?></td>
                </tr>
                <tr>
                  <td class="td-2"><?= $asignatura->indices->i_05 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_06 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_07 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_08 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_09 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_10 ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_np ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_s ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_b ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_mb ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_ac ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_co ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_na ?>%</td>
                  <td class="td-2"><?= $asignatura->indices->i_re ?>%</td>
                <?php else: ?>
                    <td class="td-text td-2"><?= $asignatura->id . ' - ' . $asignatura->nombre ?></td>
                    <td colspan="16" class="td-2">SIN CALIFICACIONES</td>
                <?php endif; ?>
            </tr>
              <?php endforeach; ?>
              <!--<div id=chart-<?php //$registroTipo->listaAsignatura[0]->idPlanEstudios ?>></div>
              <script>
                var registroTipo = <?php//json_encode($registroTipo)?>;
                var planEstudios = <?php//json_encode($planEstudio)?>;
                console.log(registroTipo);
                series = [{
                  name:"05",
                  data:[]
                },{
                  name:"06",
                  data:[]
                },{
                  name:"07",
                  data:[]
                },{
                  name:"08",
                  data:[]
                },{
                  name:"09",
                  data:[]
                },{
                  name:"10",
                  data:[]
                },{
                  name:"NP",
                  data:[]
                },{
                  name:"S",
                  data:[]
                },{
                  name:"B",
                  data:[]
                },{
                  name:"MB",
                  data:[]
                },{
                  name:"AC",
                  data:[]
                },{
                  name:"CO",
                  data:[]
                },{
                  name:"NA",
                  data:[]
                },{
                  name:"RE",
                  data:[]
                }
                ];
                categories = [];
                registroTipo.listaAsignatura.forEach(function(asignatura){
                  series[0].data.push(asignatura.indices.i_05);
                  series[1].data.push(asignatura.indices.i_06);
                  series[2].data.push(asignatura.indices.i_07);
                  series[3].data.push(asignatura.indices.i_08);
                  series[4].data.push(asignatura.indices.i_09);
                  series[5].data.push(asignatura.indices.i_10);
                  series[6].data.push(asignatura.indices.i_np);
                  series[7].data.push(asignatura.indices.i_s);
                  series[8].data.push(asignatura.indices.i_b);
                  series[9].data.push(asignatura.indices.i_mb);
                  series[10].data.push(asignatura.indices.i_ac);
                  series[11].data.push(asignatura.indices.i_co);
                  series[12].data.push(asignatura.indices.i_na);
                  series[13].data.push(asignatura.indices.i_re);
                  categories.push(asignatura.nombre);
                  console.log(series);
                });
                var options_calificaciones = {
                  series: series,
                  title: {
                    text: 'Alumnos en Módulos de Especialización'
                  },
                  legend: {
                    show: true,
                    showForSingleSeries: true
                  },
                  xaxis: {
                    categories : categories
                  },
                  chart: {
                  type: 'bar'
                  },
                plotOptions: {
                  bar: {
                    horizontal: false,
                    dataLabels: {
                      position: 'top',
                      }
                    }
                  },
                  dataLabels: {
                    enabled: true,
                    textAnchor: 'start',
                    formatter: function (val, opt) {
                      return val + "%"
                    },
                    offsetX: 0,
                    dropShadow: {
                      enabled: true
                    }
                  }
                }
                var chart = new ApexCharts(document.querySelector("#chart-<?php// $registroTipo->listaAsignatura[0]->idPlanEstudios ?>"), options_calificaciones);
                chart.render();
              </script>-->
          <?php endif; ?>
      <?php endforeach; ?>
  </table>
  <br><br>

<?php endforeach; ?>

