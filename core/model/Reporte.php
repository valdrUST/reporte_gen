<?php
require_once 'Conexion_BD.php';
    class Reporte{
        private $db;
        function __construct(){
            $this->db = new Conexion();
        }

        function get_reporte_gen($id_semestre,$id_plan_fi,$ingreso_fi,$id_carrera){
            $query = "select * from resumen_plan_generacion where id_semestre=:id_semestre and id_plan_fi=:id_plan_fi and ingreso_fi=:ingreso_fi and id_carrera=:id_carrera";
            $stmt = $this->db->prepare($query);
            $stmt->bindValue(":id_semestre",$id_semestre);
            $stmt->bindValue(":id_plan_fi",$id_plan_fi);
            $stmt->bindValue(":ingreso_fi",$ingreso_fi);
            $stmt->bindValue(":id_carrera",$id_carrera);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS);
        }
    }