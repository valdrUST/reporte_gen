<?php
/*
*   Esta clase esta para procesar todo lo relacionado a graficas de excel
*/
class LectorExcel{ 
    public $objPHPExcel;
    function __construct(){
        $this->objPHPExcel = new PHPExcel();
    }

    public function readExcel2017($file){
        $objReader = new PHPExcel_Reader_Excel2007();
        $objReader->setReadDataOnly(False);
        $this->objPHPExcel = $objReader->load($file);
    }


    public function cellColor($cells,$color){
        $this->objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('argb' => $color)));
    }
    
    public function fontColor($cells,$color){
        $styleArray = array(
            'font'  => array(
                'color' => array('rgb' => 'FFFFFF'),
                'size'  => 11,
                'name'  => 'calibri'
        ));
        $this->objPHPExcel->getActiveSheet()->getStyle($cells)->applyFromArray($styleArray);
        $this->objPHPExcel->getActiveSheet()->getStyle($cells)->getAlignment()->setWrapText(true);
    }

    public function set_percentile_format($cells){
        $this->objPHPExcel->getActiveSheet()->getStyle($cells)
        ->getNumberFormat()->applyFromArray( 
            array( 
                'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
            )
        );
    }

    public function conditional_format($cells,$color_scale){
        $data_arr = $this->objPHPExcel->getActiveSheet()->rangeToArray($cells);
        $data = array();
        foreach ($data_arr as $dat) {
            array_push($data,$dat[0]);
        }
        $scale = max($data) - min($data);
        $scale_step = $scale / count($color_scale);
        $scale_sum = min($data);
        $scale_arr = array();
        $scale_sum = $scale_sum + $scale_step;
        while($scale_sum <= max($data)){
            array_push($scale_arr,$scale_sum);
            $scale_sum = $scale_sum + $scale_step;
        }
        $data_arr = $this->objPHPExcel->getActiveSheet()->rangeToArray($cells,NULL,FALSE,FALSE,TRUE);
        //echo json_encode($data_arr);
        $rows = array_keys($data_arr);
        $row_c = 0;
        //$this->print_json($scale_arr);
        foreach ($data_arr as $dat) {
            $row = $rows[$row_c];
            $cols = array_keys($dat);
            foreach ($cols as $col) {
                $cell = $col.$row;
                $cell_dat = $data_arr = $this->objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                    $ant_val=0;
                    $next_val=$ant_val+1;
                    
                    while($next_val < count($color_scale)){
                        if($cell_dat < $scale_arr[0]){
                            $this->cellColor($cell,$color_scale[0]);
                            break;
                        }else
                        if($cell_dat > $scale_arr[$ant_val] && $cell_dat < $scale_arr[$next_val]){
                            $this->cellColor($cell,$color_scale[$ant_val]);
                            break;
                        }else 
                        if($cell_dat >= $scale_arr[count($color_scale)-1]){

                        }
                        $ant_val++;
                        $next_val = $ant_val + 1;
                    }
                    $this->cellColor($cell,$color_scale[$ant_val]);

            }
            $row_c++;
        }
    }

    public function new_sheet($name,$id){
        $this->objPHPExcel->createSheet($id);
        $sheet = $this->objPHPExcel->setActiveSheetIndex($id);
        $sheet->setTitle("$name");
    }

    public function print_excel($name_file){
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$name_file.'"');
        header('Cache-Control: max-age=0');
        $objWriter = new PHPExcel_Writer_Excel2007($this->objPHPExcel);
        $objWriter->setIncludeCharts(TRUE);
        ob_end_clean();
        $objWriter->save("php://output");  
    }

    public function radial_graph($labels,$xaxisTickValues,$values,$values_tam_row,$topLeft,$bottomRight,$sheetName,$title_n){
        $dataseriesLabels = array();
        foreach ($labels as $label) {
            array_push($dataseriesLabels,new PHPExcel_Chart_DataSeriesValues('String', $label, NULL, 1));
        }
        $xaxisTickValues_graph = array();
        foreach ($xaxisTickValues as $xaxisTickValue){
            array_push($xaxisTickValues_graph, new PHPExcel_Chart_DataSeriesValues('String',$xaxisTickValue, NULL, $values_tam_row));
        }
        
        $dataSeriesValues = array();
        foreach ($values as $value){
            array_push($dataSeriesValues, new PHPExcel_Chart_DataSeriesValues('Number',$value, NULL, $values_tam_row));
        }
        $series = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_RADARCHART,				// plotType
            NULL,													// plotGrouping
            range(0, count($dataSeriesValues)-1),					// plotOrder
            $dataseriesLabels,										// plotLabel
            $xaxisTickValues_graph,										// plotCategory
            $dataSeriesValues,										// plotValues
            NULL,						
            NULL,							// smooth line
            PHPExcel_Chart_DataSeries::STYLE_MARKER					// plotStyle
        );
        $layout = new PHPExcel_Chart_Layout();

        //	Set the series in the plot area
        $plotarea = new PHPExcel_Chart_PlotArea($layout, array($series));
        //	Set the chart legend
        $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT,NULL, false);
        $title = new PHPExcel_Chart_Title($title_n);
        //	Create the chart
        $chart = new PHPExcel_Chart(
            'radar',		// name
            $title,			// title
            $legend,		// legend
            $plotarea,		// plotArea
            true,			// plotVisibleOnly
            NULL,				// displayBlanksAs
            NULL,			// xAxisLabel
            NULL			// yAxisLabel		- Radar charts don't have a Y-Axis
        );
        $chart->setTopLeftPosition($topLeft);
        $chart->setBottomRightPosition($bottomRight);
        $this->objPHPExcel->setActiveSheetIndexByName($sheetName)->addChart($chart);
    }

    
    public function donut_pie_graph($labels,$xaxisTickValues,$values,$values_tam_row,$topLeft,$bottomRight,$sheetName,$title_n){
        $dataseriesLabels = array();
        foreach ($labels as $label) {
            array_push($dataseriesLabels,new PHPExcel_Chart_DataSeriesValues('String', $label, NULL, 1));
        }
        $xaxisTickValues_graph = array();
        foreach ($xaxisTickValues as $xaxisTickValue){
            array_push($xaxisTickValues_graph, new PHPExcel_Chart_DataSeriesValues('String',$xaxisTickValue, NULL, $values_tam_row));
        }
        
        $dataSeriesValues = array();
        foreach ($values as $value){
            array_push($dataSeriesValues, new PHPExcel_Chart_DataSeriesValues('Number',$value, PHPExcel_Chart_Properties::FORMAT_CODE_PERCENTAGE, $values_tam_row));
        }
        $series = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_DONUTCHART,				// plotType
            NULL,													// plotGrouping
            range(0, count($dataSeriesValues)-1),					// plotOrder
            $dataseriesLabels,										// plotLabel
            $xaxisTickValues_graph,										// plotCategory
            $dataSeriesValues,										// plotValues
            NULL,						
            NULL,							// smooth line
            NULL
        );
        $layout = new PHPExcel_Chart_Layout();  
        $layout->setShowPercent(True);

        //	Set the series in the plot area
        $plotarea = new PHPExcel_Chart_PlotArea($layout, array($series));
        //	Set the chart legend
        $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
        $title = new PHPExcel_Chart_Title($title_n);
        //	Create the chart
        $chart = new PHPExcel_Chart(
            'radar',		// name
            $title,			// title
            $legend,		// legend
            $plotarea,		// plotArea
            true,			// plotVisibleOnly
            0,				// displayBlanksAs
            NULL,			// xAxisLabel
            NULL			// yAxisLabel		- Radar charts don't have a Y-Axis
        );
        $chart->setTopLeftPosition($topLeft);
        $chart->setBottomRightPosition($bottomRight);
        $this->objPHPExcel->setActiveSheetIndexByName($sheetName)->addChart($chart);
    }

    public function stacked_bar_graph($labels,$xaxisTickValues,$values,$values_tam_row,$topLeft,$bottomRight,$sheetName,$title_n){
        $dataseriesLabels = array();
        foreach ($labels as $label) {
            array_push($dataseriesLabels,new PHPExcel_Chart_DataSeriesValues('String', $label, NULL, 1));
        }
        $xaxisTickValues_graph = array();
        foreach ($xaxisTickValues as $xaxisTickValue){
            array_push($xaxisTickValues_graph, new PHPExcel_Chart_DataSeriesValues('Number',$xaxisTickValue, PHPExcel_Chart_Properties::FORMAT_CODE_PERCENTAGE, $values_tam_row));
        }
        
        $dataSeriesValues = array();
        foreach ($values as $value){
            array_push($dataSeriesValues, new PHPExcel_Chart_DataSeriesValues('String',$value, PHPExcel_Chart_Properties::FORMAT_CODE_PERCENTAGE, $values_tam_row));
        }
        $yAxisLabel = new PHPExcel_Chart_Title('%');
        $series = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
	        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,	// plotGrouping
            range(0, count($dataSeriesValues)-1),					// plotOrder
            $dataseriesLabels,										// plotLabel
            NULL,										// plotCategory
            $dataSeriesValues,										// plotValues
            PHPExcel_Chart_DataSeries::DIRECTION_BAR
        );
        $plotArea = new PHPExcel_Chart_PlotArea(NULL, array($series));
        //	Set the chart legend
        $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);
        $title = new PHPExcel_Chart_Title($title_n);
        $axis =  new PHPExcel_Chart_Axis();
        $axis->setAxisOptionsProperties('nextTo', null, null, null, null, null, 0, 1, .2, .1);
        $chart = new PHPExcel_Chart(
            'Stacked Bar',		// name
            $title,			// title
            $legend,		// legend
            $plotArea,		// plotArea
            true,			// plotVisibleOnly
            0,				// displayBlanksAs
            NULL,			// xAxisLabel
            $yAxisLabel,		// yAxisLabel
            $axis
        );
        
        //	Set the position where the chart should appear in the worksheet
        $chart->setTopLeftPosition($topLeft);
        $chart->setBottomRightPosition($bottomRight);
        $this->objPHPExcel->setActiveSheetIndexByName($sheetName)->addChart($chart);
    }

    public function standar_bar_graph($labels,$xaxisTickValues,$values,$values_tam_row,$topLeft,$bottomRight,$sheetName,$title_n){
        $dataseriesLabels = array();
        foreach ($labels as $label) {
            array_push($dataseriesLabels,new PHPExcel_Chart_DataSeriesValues('String', $label, NULL, 1));
        }
        $xaxisTickValues_graph = array();
        foreach ($xaxisTickValues as $xaxisTickValue){
            array_push($xaxisTickValues_graph, new PHPExcel_Chart_DataSeriesValues('Number',$xaxisTickValue, PHPExcel_Chart_Properties::FORMAT_CODE_PERCENTAGE, $values_tam_row));
        }
        
        $dataSeriesValues = array();
        foreach ($values as $value){
            array_push($dataSeriesValues, new PHPExcel_Chart_DataSeriesValues('String',$value, PHPExcel_Chart_Properties::FORMAT_CODE_PERCENTAGE, $values_tam_row));
        }
        $yAxisLabel = new PHPExcel_Chart_Title('%');
        $series = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
	        NULL,	// plotGrouping
            range(0, count($dataSeriesValues)-1),					// plotOrder
            $dataseriesLabels,										// plotLabel
            NULL,										// plotCategory
            $dataSeriesValues										// plotValues
        );
        $plotArea = new PHPExcel_Chart_PlotArea(NULL, array($series));
        //	Set the chart legend
        $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_TOP, NULL, false);
        $title = new PHPExcel_Chart_Title($title_n);
        $axis =  new PHPExcel_Chart_Axis();
        $axis->setAxisOptionsProperties('nextTo', null, null, null, null, null, 0, 1, .2, .1);
        $chart = new PHPExcel_Chart(
            'Stacked Bar',		// name
            $title,			// title
            $legend,		// legend
            $plotArea,		// plotArea
            true,			// plotVisibleOnly
            0,				// displayBlanksAs
            NULL,			// xAxisLabel
            $yAxisLabel,	// yAxisLabel
            $axis
        );
        
        //	Set the position where the chart should appear in the worksheet
        $chart->setTopLeftPosition($topLeft);
        $chart->setBottomRightPosition($bottomRight);
        $this->objPHPExcel->setActiveSheetIndexByName($sheetName)->addChart($chart);
    }

    public function standar_group_bar_graph($labels,$xaxisTickValues,$values,$values_tam_row,$topLeft,$bottomRight,$sheetName,$title_n){
        $dataseriesLabels = array();
        foreach ($labels as $label) {
            array_push($dataseriesLabels,new PHPExcel_Chart_DataSeriesValues('String', $label, NULL, 1));
        }
        $xaxisTickValues_graph = array();
        foreach ($xaxisTickValues as $xaxisTickValue){
            array_push($xaxisTickValues_graph, new PHPExcel_Chart_DataSeriesValues('String',$xaxisTickValue, NULL, $values_tam_row));
        }
        $dataSeriesValues = array();
        foreach ($values as $value){
            array_push($dataSeriesValues, new PHPExcel_Chart_DataSeriesValues('Number',$value, PHPExcel_Chart_Properties::FORMAT_CODE_PERCENTAGE, $values_tam_row));
        }
        $yAxisLabel = new PHPExcel_Chart_Title('%');
        $series = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_BARCHART,		// plotType
	        PHPExcel_Chart_DataSeries::GROUPING_STANDARD,	// plotGrouping
            range(0, count($dataSeriesValues)-1),					// plotOrder
            $dataseriesLabels,										// plotLabel
            $xaxisTickValues_graph,										// plotCategory
            $dataSeriesValues										// plotValues
        );
        $plotArea = new PHPExcel_Chart_PlotArea(NULL, array($series));
        //	Set the chart legend
        $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_TOP, NULL, false);
        $title = new PHPExcel_Chart_Title($title_n);
        $axis =  new PHPExcel_Chart_Axis();
        $axis->setAxisOptionsProperties('nextTo', null, null, null, null, null, 0, 1, .2, .1);
        $chart = new PHPExcel_Chart(
            'Grouped Bar',		// name
            $title,			// title
            $legend,		// legend
            $plotArea,		// plotArea
            true,			// plotVisibleOnly
            0,				// displayBlanksAs
            NULL,			// xAxisLabel
            $yAxisLabel,		// yAxisLabel
            $axis
        );

        //	Set the position where the chart should appear in the worksheet
        $chart->setTopLeftPosition($topLeft);
        $chart->setBottomRightPosition($bottomRight);
        $chart->setBottomRightYOffset(100);
        $chart->setBottomRightXOffset(100);
        $this->objPHPExcel->setActiveSheetIndexByName($sheetName)->addChart($chart);
    }

    public function standar_line_graph($labels,$xaxisTickValues,$values,$values_tam_row,$topLeft,$bottomRight,$sheetName,$title_n){
        $dataseriesLabels = array();
        foreach ($labels as $label) {
            array_push($dataseriesLabels,new PHPExcel_Chart_DataSeriesValues('String', $label, NULL, 1));
        }
        $xaxisTickValues_graph = array();
        foreach ($xaxisTickValues as $xaxisTickValue){
            array_push($xaxisTickValues_graph, new PHPExcel_Chart_DataSeriesValues('String',$xaxisTickValue, NULL, $values_tam_row));
        }
        
        $dataSeriesValues = array();
        foreach ($values as $value){
            array_push($dataSeriesValues, new PHPExcel_Chart_DataSeriesValues('Number',$value, NULL, $values_tam_row));
        }
        $yAxisLabel = new PHPExcel_Chart_Title('Alumnos');
        $xAxisLabel = new PHPExcel_Chart_Title('Semestre');
        $series = new PHPExcel_Chart_DataSeries(
            PHPExcel_Chart_DataSeries::TYPE_LINECHART,		// plotType
	        NULL,	// plotGrouping
            range(0, count($dataSeriesValues)-1),					// plotOrder
            $dataseriesLabels,										// plotLabel
            $xaxisTickValues_graph,										// plotCategory
            $dataSeriesValues										// plotValues
        );
        $plotArea = new PHPExcel_Chart_PlotArea(NULL, array($series));
        //	Set the chart legend
        $legend = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_BOTTOM, NULL, false);
        $title = new PHPExcel_Chart_Title($title_n);
        $chart = new PHPExcel_Chart(
            'Line chart',		// name
            $title,			// title
            $legend,		// legend
            $plotArea,		// plotArea
            true,			// plotVisibleOnly
            0,				// displayBlanksAs
            $xAxisLabel,	// xAxisLabel
            $yAxisLabel		// yAxisLabel
        );
        
        //	Set the position where the chart should appear in the worksheet
        $chart->setTopLeftPosition($topLeft);
        $chart->setBottomRightPosition($bottomRight);
        $chart->setBottomRightYOffset(100);
        $chart->setBottomRightXOffset(100);
        $this->objPHPExcel->setActiveSheetIndexByName($sheetName)->addChart($chart);
    }

    public function create_hidden_sum($range_sum,$pos_out,$type='row',$sheetName){
        $this->objPHPExcel->setActiveSheetIndexByName($sheetName);
        $data_range = $this->objPHPExcel->getActiveSheet()->rangeToArray($range_sum,NULL,FALSE,FALSE,TRUE);
        //echo json_encode($data_arr);
        $col_out = $pos_out[0];
        $row_out = $pos_out[1];
        foreach (array_values($data_range) as $row) {
            $sum = 0;
            foreach (array_values($row) as $value) {
                $sum += $value;
            }
            $this->objPHPExcel->getActiveSheet()->SetCellValue($col_out.''.$row_out, $sum);
            $this->fontColor($col_out.''.$row_out,'FFFFFF');
            $row_out++;
        }
        //$this->objPHPExcel->setCellValue("");
    }
    
    public function create_plan_grafico_sheet_template($name_in){
        $id = $this->objPHPExcel->setActiveSheetIndexByName($name_in);
        $id = $this->objPHPExcel->getActiveSheetIndex();
        $tempSheet = $this->objPHPExcel->setActiveSheetIndexByName($name_in)->copy();
        $tempSheet ->setTitle("$name_in gráfico");
        $this->objPHPExcel->addSheet($tempSheet);
        unset($tempSheet);
        $this->objPHPExcel->setIndexByName("$name_in gráfico",$id+1);
        $h_row = $this->objPHPExcel->getActiveSheet()->getHighestRow();
        $cells = $this->objPHPExcel->setActiveSheetIndexByName("$name_in gráfico")->rangeToArray('C1:C'.$h_row,NULL,TRUE,FALSE,TRUE);
        $range_blank = array();
        $c_row = 1;
        foreach ($cells as $cell) {
            if(preg_match("/Semestre/i",$cell['C']) || preg_match("/Optativa/i",$cell['C'])){
                array_push($range_blank,$c_row-1);
                array_push($range_blank,$c_row+1);
            }
            $c_row++;
        }
        array_push($range_blank,intval($h_row));
        $range_rows = array();
        unset($range_blank[0]);
        $range = array();
        foreach ($range_blank as $row_p) {
            array_push($range,$row_p);
            if(count($range) == 2){
                array_push($range_rows,$range);
                $range = array();
            }
        }
        foreach ($range_rows as $range) {
            $num_rows = $range[1] - $range[0];
            $labels = array();
            $values = array();
            $xaxisTickValues = array(
                '\''."$name_in gráfico".'\'!$F$'.$range[0].':$S$'.$range[0]
            );
            $par = 1;
            for($i = $range[0]; $i<=$range[1] ; $i++){
                if($par % 2 != 0){
                    if($i<$range[1]){
                        array_push($values,'\''."$name_in gráfico".'\'!$F$'.$i.':$S$'.$i);
                    }
                }else{
                    if($i<$range[1])
                       array_push($labels,'\''."$name_in gráfico".'\'!$C$'.$i);
                }
                try{
                    $this->objPHPExcel->getActiveSheet()->unmergeCells("C$i:S$i");
                }catch(Exception $e){
                    
                }
                $this->cellColor("C$i:S$i",'FFFFFF');
                $this->fontColor("C$i:S$i",'FFFFFF');
                $par++;
            }
            if($range[0] < $range[1])
                $this->standar_group_bar_graph($labels,$xaxisTickValues,$values,14,"C$range[0]","S$range[1]","$name_in gráfico","calificaciones");
        }
    }

    public function print_json($obj){
        header('Content-Type: application/json');
        echo(json_encode($obj));
    }
}
