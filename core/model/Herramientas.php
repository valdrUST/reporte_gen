<?php
/** @noinspection RegExpRedundantEscape */
/** @noinspection PhpUnusedParameterInspection */
/** @noinspection PhpIncludeInspection */
/** @noinspection PhpUndefinedClassInspection */

/**
 * Clase Herramientas
 *
 * Métodos y funciones del Framework.
 *
 * @package    Framework-USECAD
 * @subpackage Sistema
 * @category   Core
 * @author     Unidad de Servicios de Cómputo Administrativos
 * @since      Versión 1.0.0
 */
class Herramientas
{
    /**
     * Función castStdToCustom
     *
     * Convierte un "Objeto de clase estándar" en un "Objeto de la clase indicada."
     *
     * @param string $nombreClase Nombre de la clase que se desea obtener como resultado
     * @param object $standardObject Objeto a convertir
     *
     * @return object Objeto de la clase indicada
     *
     * @throws CastException Excepción cuando existe una falla al convertir archivos
     */
    public static function castStdToCustom($nombreClase, $standardObject)
    {
        // Detecta si se ha recibido un objeto.
        if (!is_object($standardObject)) {
            // No se recibió un objeto, envía una excepción de tipo no valido.
            throw new CastException('El elemento no es un objeto, no puede ser convertido.');
        }

        // Agrega el sufijo Modelo a la Clase recibida
        $nombreClase .= 'Modelo';

        // Detecta si la clase ha sido definida para la aplicación.
        if (!class_exists($nombreClase)) {
            // La clase indicada no ha sido definida, envía una excepción.
            throw new CastException('La Clase ' . $nombreClase . ' no ha sido definida.');
        }

        // Crea un objeto de la clase indicada.
        $objeto = new $nombreClase;

        // Aplica una reflexión al objeto estándar recibido para poder leer sus propiedades e identificar sus "tipos".
        $standardReflection = new ReflectionObject($standardObject);

        // Recorre cada propiedad del objeto estándar resultante.
        foreach ($standardReflection->getProperties() as $standardProperty) {
            // Obtiene el nombre de la propiedad del objeto.
            $nombrePropiedad = $standardProperty->getName();

            // Determina el "tipo" de la propiedad del objeto "custom" de acuerdo al nombre obtenido.
            switch (gettype($objeto->{$nombrePropiedad})) {
                case 'NULL':
                    // La propiedad no se encuentra definida en la clase indicada, enviá una excepción.
                    throw new CastException('La Clase solicitada no cuenta con la propiedad: ' . $nombrePropiedad);

                    // Sale del condicional
                    break;
                case 'object':
                    // La propiedad del objeto es a su vez otro objeto.

                    // Convierte la primera letra del nombre de la propiedad del objeto en Mayúscula.
                    $nombreClaseHija = ucwords($nombrePropiedad);

                    // Ejecuta recursivamente el método castStdToCustom.
                    $objeto->{$nombrePropiedad} = self::castStdToCustom($nombreClaseHija, $standardObject->{$nombrePropiedad});

                    // Sale del condicional
                    break;
                case 'array':
                    // Es un arreglo, convertirá cada elemento del arreglo en un objeto del tipo indicado.

                    // Inicializa el arreglo.
                    $customArray = array();

                    // Recorre cada elemento del arreglo recibido.
                    foreach ($standardObject->{$nombrePropiedad} as $key => $value) {
                        // Convierte cada elemento del arreglo en un objeto del tipo indicado.
                        //     - Agrega el objeto resultante al nuevo arreglo contenedor.
                        array_push($customArray, self::castStdToCustom(str_replace('Modelo', '', get_class($value)), $value));
                    }

                    $objeto->{$nombrePropiedad} = $customArray;

                    // Sale del condicional
                    break;
                default:
                    // La propiedad del objeto es plana.

                    // Ajusta la propiedad del objeto "custom".
                    $objeto->{$nombrePropiedad} = $standardObject->{$nombrePropiedad};

                    // Sale del condicional
                    break;
            }
        }

        return $objeto;
    }

    /**
     * Función castToCustom
     *
     * Convierte un "Objeto o Arreglo" en un "Objeto o Arreglo de objetos de la clase indicada".
     *
     * @param string $nombreClase Nombre de la clase que se desea obtener como resultado
     * @param object $standardObject Objeto a convertir
     *
     * @return object|array Objeto o Arreglo de objetos de la clase indicada
     *
     * @throws CastException Excepción personalizada para fallas al convertir objetos
     */
    public static function castToCustom($nombreClase, $standardObject)
    {
        // Determina el tipo de elemento a convertir.
        if (is_object($standardObject)) {
            // Es un objeto.

            // Convierte el objeto recibido en un objeto del tipo indicado.
            $custom = self::castStdToCustom($nombreClase, $standardObject);
        } elseif (is_array($standardObject)) {
            // Es un arreglo, convertirá cada elemento del arreglo en un objeto del tipo indicado.

            // Inicializa el arreglo.
            $custom = array();

            // Recorre cada elemento del arreglo recibido.
            foreach ($standardObject as $key => $value) {
                // Convierte cada elemento del arreglo en un objeto del tipo indicado.
                //     - Agrega el objeto resultante al nuevo arreglo contenedor.
                array_push($custom, self::castStdToCustom($nombreClase, $value));
            }
        } else {
            // No es un tipo de dato valido, envía una excepción.
            throw new CastException('El elemento recibido no puede ser convertido debido a que no es un arreglo o un objeto.');
        }

        return $custom;
    }

    /**
     * Función validarAcceso
     *
     * Determina si la petición es aceptada de acuerdo al nivel de acceso.
     *
     * @param boolean $sesionIniciada Indica la existencia de una sesión de usuario
     * @param object $usuarioModelo Objeto que representa un Usuario
     * @param string $nivelAcceso Cadena que representa el nivel de acceso solicitado (Primer subdirectorio)
     * @param boolean $zonaPrivada Indica si el nivel de acceso soliictado se encentra en la Zona Privada
     *
     * @throws ConfigException Excepción por error de configuración
     * @throws BaseDatosException Excepción por error al interactar con la Base de Datos
     * @throws AuthException Excepción cuando la petición no est autorizada
     */
    public static function validarAcceso($sesionIniciada, $usuarioModelo, $nivelAcceso, $zonaPrivada)
    {
        // Determina si el nivel de acceso solicitado se encuentra en los accesos restringidos.
        if ($zonaPrivada) {
            // La solicitud se encuentra en la zona restringida.

            // Valida la sesión del usuario.
            UsuarioModelo::validarSesion($sesionIniciada, $usuarioModelo, $nivelAcceso);
        } elseif ($nivelAcceso == 'renovar') {
            // La solicitud se encuentra fuera de la zona restringida, pero debe validar la sesión del usuario.

            // Valida la sesión del usuario (sin importar el nivel de acceso).
            UsuarioModelo::validarSesion($sesionIniciada, $usuarioModelo, null);
        }
    }

    /**
     * Función obtenerFechaPeriodoGracia
     *
     * Obtiene una fecha de inicio o fin con su respectivo Periodo de Gracia.
     *
     * @param string $fecha Fecha por convertir
     * @param string $tipo Tipo de periodo de gracia (APERTURA o CIERRE)
     * @param string $periodoGraciaPersonalizado (Opcional) Periodo de gracia personalizado
     *
     * @return datetime Objeto con la fecha resultante
     *
     * @throws Exception Excepción por trabajo con fechas
     */
    public static function obtenerFechaPeriodoGracia($fecha, $tipo, $periodoGraciaPersonalizado = null)
    {
        $fechaGracia = new DateTime($fecha);

        if ($tipo === 'APERTURA') {
            $minutos = empty($periodoGraciaPersonalizado) ? PERIODO_GRACIA_APERTURA : $periodoGraciaPersonalizado;
            $fechaGracia->sub(new DateInterval('PT' . $minutos . 'M'));
        } elseif ($tipo === 'CIERRE') {
            $minutos = empty($periodoGraciaPersonalizado) ? PERIODO_GRACIA_CIERRE : $periodoGraciaPersonalizado;
            $fechaGracia->add(new DateInterval('PT' . $minutos . 'M'));
        }

        return $fechaGracia;
    }

    /**
     * Función obtenerDisponibilidadModuloPorCalendario
     *
     * Obtiene la disponibilidad de un Módulo en funcion de su Calendario de Disponbilidad.
     *
     * @param string $nombreModulo Cadena que representa el nombre del Módulo por validar
     * @param array $calendarioDisponibilidad Arreglo que contiene el Calendario de Disponibilidad
     * @param datetime $ahora Fecha del sistema
     *
     * @return array Arreglo con los datos de la disponibilidad del periodo
     *
     * @throws Exception Excepción por manipulación de fechas
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     */
    public static function obtenerDisponibilidadModuloPorCalendario($nombreModulo, $calendarioDisponibilidad, $ahora)
    {
        // Inicializa los datos para la etapa en la que se encuentra
        $datosPeriodo = array();

        if (empty($calendarioDisponibilidad)) {
            // Establece la bandera que indica que el módulo si esta disponible
            $datosPeriodo['disponible'] = true;
        } else {
            // Determina si el calendario de fechas es un arreglo
            if (!is_array($calendarioDisponibilidad)) {
                throw new ConfigException('El calendario de disponibilidad para el módulo ' . $nombreModulo . ' no se encuentra definido correctamente.');
            }

            // Recorre cada una de las fechas de disponibilidad para determinar si la interfaz debe estar activa
            foreach ($calendarioDisponibilidad as $periodo) {
                // Recupera la hora de inicio y fin indicada
                $inicio = self::obtenerFechaPeriodoGracia($periodo['fecha_inicio'], 'APERTURA', $periodo['periodo_gracia_apertura']);
                $fin = self::obtenerFechaPeriodoGracia($periodo['fecha_fin'], 'CIERRE', $periodo['periodo_gracia_cierre']);

                $inicioTexto = self::convertirFechaHoraCadena(new DateTime($periodo['fecha_inicio']));
                $finTexto = self::convertirFechaHoraCadena(new DateTime($periodo['fecha_fin']));

                // Ajusta la cadena de calendario de disponibilidad
                $datosPeriodo['textoCalendario'] .= '<br><strong>' . $inicioTexto . '</strong> a <strong>' . $finTexto . '</strong>';

                // Determina si debe sobrescribir el nombre del periodo
                if ($periodo['esProrroga']) {
                    $periodo['nombrePeriodo'] = 'prórroga';
                }

                // Ajuste y validación cuando el Nombre del Periodo debe ser mostrado
                if ($periodo['mostrarPeriodo']) {
                    if (empty($periodo['nombrePeriodo'])) {
                        throw new ConfigException('El nombre del periodo para el módulo ' . $nombreModulo . ' no ha sido definido correctamente.');
                    }

                    // Concatena el nombre del periodo al calendario de disponibilidad
                    $datosPeriodo['textoCalendario'] .= ' (' . $periodo['nombrePeriodo'] . ')';
                }

                // Determina si la fecha y hora actual se encuentra entre los rangos de tiempo deifnidos
                if ($ahora >= $inicio && $ahora <= $fin) {
                    // Establece la bandera que indica que el módulo si esta disponible
                    $datosPeriodo['disponible'] = true;

                    // Establece la etapa en la que se encuentra
                    $datosPeriodo['etapa'] = $periodo['nombrePeriodo'];

                    // Sale del condicional
                    break;
                }
            }
        }

        return $datosPeriodo;
    }

    /**
     * Función obtenerDisponibilidadModuloPorListaUsuario
     *
     * Obtiene la disponibilidad de un Módulo en funcion de su Lista de Usuarios Permitidos.
     *
     * @param string $nombreModulo Cadena que representa el nombre del Módulo por validar
     * @param array $listaBlancaUsuario Arreglo que contiene la lista de usuarios permitidos
     * @param string $nombreUsuario Cadena que representa el nombre del Usuario
     *
     * @return Boolean Bandera que define la disponibilidad del módulo
     *
     * @throws Exception Excepción por manipulación de fechas
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     */
    public static function obtenerDisponibilidadModuloPorListaUsuario($nombreModulo, $listaBlancaUsuario, $nombreUsuario)
    {
        if (empty($listaBlancaUsuario)) {
            // Establece la bandera que indica que el módulo si esta disponible
            $disponible = true;
        } else {
            // Determina si el calendario de fechas es un arreglo
            if (!is_array($listaBlancaUsuario)) {
                throw new ConfigException('La lista de usuarios para el módulo ' . $nombreModulo . ' no se encuentra definida correctamente.');
            }

            $disponible = (in_array($nombreUsuario, $listaBlancaUsuario)) ? true : false;
        }

        return $disponible;
    }

    /**
     * Función obtenerDisponibilidadModuloPorListaIP
     *
     * Obtiene la disponibilidad de un Módulo en funcion de su Lista de Direcciones IP Permitidas.
     *
     * @param string $nombreModulo Cadena que representa el nombre del Módulo por validar
     * @param array $listaBlancaIP Arreglo que contiene la lista de usuarios permitidos
     *
     * @return Boolean Bandera que define la disponibilidad del módulo
     *
     * @throws Exception Excepción por manipulación de fechas
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     */
    public static function obtenerDisponibilidadModuloPorListaIP($nombreModulo, $listaBlancaIP)
    {
        if (empty($listaBlancaIP)) {
            // Establece la bandera que indica que el módulo si esta disponible
            $disponible = true;
        } else {
            // Determina si el calendario de fechas es un arreglo
            if (!is_array($listaBlancaIP)) {
                throw new ConfigException('La lista de acceso para el módulo ' . $nombreModulo . ' no se encuentra definida correctamente.');
            }

            $disponible = (in_array(AppHerramientas::obtenerIPOrigenSolicitud(), $listaBlancaIP)) ? true : false;
        }

        return $disponible;
    }

    /**
     * Función determinarMenu
     *
     * Determina el tipo de menú que debe ser utilizado.
     *
     * Varía en función de:
     *     - Sesión de usuario.
     *     - Zona de consulta.
     *
     * @param boolean $sesionIniciada Indica la existencia de una sesión de usuario
     * @param object $usuarioModelo Objeto que representa un Usuario
     * @param string $nivelAcceso Cadena que representa el nivel de acceso solicitado (Primer subdirectorio)
     * @param boolean $zonaPrivada Indica si el nivel de acceso soliictado se encentra en la Zona Privada
     *
     * @return array Arreglo con la definición del menú que debe ser utilizado
     *
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     */
    public static function determinarMenu($sesionIniciada, $usuarioModelo, $nivelAcceso, $zonaPrivada)
    {
        // Construye la ruta del archivo donde se define el Menú de la Aplicación
        $archivoMenu = __DIR__ . '/../../aplicacion/configuracion/AppMenu.php';

        // Determina si el archivo existe
        if (!file_exists($archivoMenu)) {
            throw new ConfigException('No se encuentra el archivo que define el menú.');
        }

        // Importa el archivo que define el Menú de la Aplicación
        $appMenu = array();
        require $archivoMenu;

        // Determina el menu que debe ser utilizado.
        if ($sesionIniciada) {
            // Existe una sesión de usuario.

            // Determina si el usuario tiene elegido un perfil de acceso
            if (!empty($usuarioModelo->perfil)) {
                // El usuario tiene una sesión de usuario abierta y con un perfil de acceso seleccionado

                // Determina si el nivel de acceso solicitado se encuentra en los accesos restringidos.
                if ($zonaPrivada) {
                    // La solicitud se encuentra en la zona restringida.

                    // Debe utilizar el menú para la zona privada de acuerdo a su perfil de acceso.
                    $menu = $appMenu[$usuarioModelo->perfil];
                } else {
                    // La solicitud se encuentra en la zona pública.

                    // Debe utilizar el menú para la zona pública.
                    $menu = $appMenu['publico'];
                }
            } else {
                // El no tiene un perfil de acceso seleccionado (debe especificar algún perfil de los disponibles)

                // Debe utilizar el menú para la zona pública.
                $menu = $appMenu['publico'];
            }
        } else {
            // No existe una sesión de usuario.

            // Determina si el nivel de acceso solicitado se encuentra en los accesos restringidos.
            if ($zonaPrivada) {
                // La solicitud se encuentra en la zona restringida.

                // Utilza el menú para la zona privada de acuerdo al perfil indicado en la URI
                $menu = $appMenu[$nivelAcceso];
            } else {
                // Debe utilizar el menú para la zona privada.
                $menu = $appMenu['publico'];
            }
        }

        // Destruye los objetos y variables auxiliares creados
        unset($appMenu);

        // Determina si existe un menu asociado para la zona
        if (!empty($menu)) {
            // Determina si menu es un Array
            if (!is_array($menu)) {
                throw new ConfigException('El menú asociado no se encuentra definido correctamente.');
            }
        }

        return $menu;
    }

    /**
     * Función obtenerConfiguracionRuta
     *
     * Obtiene la configuración adecuada para la vista solicitada.
     *
     * @param array $menu Definicion del menú que debe ser utilizado.
     * @param datetime $ahora Fecha y hora del sistema.
     * @param array $vistaArreglo Vista solicitada por el usuario.
     * @param boolean $sesionIniciada Indica la existencia de una sesión de usuario
     * @param object $usuarioModelo Objeto que representa un Usuario
     * @param string $nivelAcceso Cadena que representa el nivel de acceso solicitado (Primer subdirectorio)
     * @param boolean $zonaPrivada Indica si el nivel de acceso soliictado se encentra en la Zona Privada
     *
     * @return  array Configuración para la vista solicitada
     *
     * @throws AplicacionExcepcion Excepción provocada al cerrar sesión
     * @throws AuthException Excepción cuando existe una falla por problemas de autenticación
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     * @throws InterfazDisponibleException Excepción cuando la aplicación no esta disponible
     * @throws ListaBlancaException Excepción cuando la aplicación no esta disponible
     */
    public static function obtenerConfiguracionRuta($menu, $ahora, $vistaArreglo, $sesionIniciada, $usuarioModelo, $nivelAcceso, $zonaPrivada)
    {
        // Determina si existe un menú asociado
        if (empty($menu)) {
            throw new ConfigException('No se ha definido una forma de comportamiento para esta página.');
        }

        // Determina si el usuario con sesion iniciada ya cuenta con un perfil de acceso
        if ($sesionIniciada && !empty($usuarioModelo->perfil)) {
            // Si el perfil de usuario se encuentra en el primer subdirectorio de la vista, elimina el perfil de usuario de la vista solicitada
            if ($nivelAcceso === $usuarioModelo->perfil) {
                $vistaArreglo = array_slice($vistaArreglo, 1);
            }
        } else {
            // Determina si el nivel de acceso solicitado se encuentra en los accesos restringidos.
            if ($zonaPrivada) {
                // La solicitud se encuentra en la zona restringida.

                // Elimina el perfil de usuario de la vista solicitada
                $vistaArreglo = array_slice($vistaArreglo, 1);
            }
        }

        // Inicializa la bandera de disponibilidad por dirección IP de acceso
        $enListaBlanca = self::obtenerDisponibilidadModuloPorListaIP($nivelAcceso, $menu['listaBlancaIP']);

        // Realiza las acciones correspondientes de acuerdo a la disponibilidad por dirección IP de acceso
        if ($enListaBlanca) {
            // Recorre cada entrada de la definición del menú
            $config = self::recorrerElementoMenu($vistaArreglo, $menu['hijos'], $ahora, $usuarioModelo->username);
        } else {
            // Limpia la sesión de usuario
            UsuarioModelo::cerrarSesion(false);

            // Arroja la excepción adecuada
            throw new ListaBlancaException('La aplicación no esta disponible desde esta ubicación.<br>Verificar que el acceso se realice desde una dirección IPv4.');
        }

        return $config;
    }

    /**
     * Función recorrerElementoMenu
     *
     * Recorre cada elemento de la definicion del menú para obtener la configuración correspondiente.
     *
     * @param array $vistaArreglo Arreglo que contiene cada elemento de la vista solicitada.
     * @param array $menu Arreglo que contiene el menú/rama de menú que debe ser recorrido.
     * @param datetime $ahora Fecha y hora del sistema.
     * @param string $nombreUsuario (Opcional) Nombre del usuario que utiliza la aplicación.
     *
     * @return array Configuración para la vista solicitada
     *
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     * @throws Exception Excepción por manipulación de fechas
     * @throws InterfazDisponibleException Excepción cuando la interfaz no esta disponible por alguna razón
     */
    public static function recorrerElementoMenu($vistaArreglo, $menu, $ahora, $nombreUsuario = null)
    {
        // Obtiene el primer elemento del arreglo que define la vista solicitada
        $primerElemento = $vistaArreglo[0];

        // Determina si el menu recibido es un arreglo
        if (!is_array($menu)) {
            throw new ConfigException('Los elementos del menú no se encuentran definidos correctamente');
        }

        // Determina si el primer elemento de la vista se encuentra definido en el menú
        if (array_key_exists($primerElemento, $menu)) {
            // Recupera la rama descendente del elemento de la vista solicitada
            $menuElemento = $menu[$primerElemento];

            // Inicializa la bandera de disponibilidad por dirección IP de acceso
            $enListaBlanca = self::obtenerDisponibilidadModuloPorListaIP($primerElemento, $menuElemento['listaBlancaIP']);

            // Realiza las acciones correspondientes de acuerdo a la disponibilidad por dirección IP de acceso
            if (!$enListaBlanca) {
                throw new InterfazDisponibleException('La página solicitada no se encuentra disponible desde esta ubicación.<br><u><i>Por favor, consulte desde un lugar autorizado.</i></u>');
            }

            // Inicializa la bandera de disponibilidad por Calendario
            $disponiblePorCalendario = false;

            // Determina la disponibilidad por Calendario en funcion de sus atributos
            if (array_key_exists('disponibilidadForzar', $menuElemento)) {
                // La entrada tiene definido el despliegue en menú de manera forzada, obedece dicho valor sin importar el calendario de disponibilidad
                $disponiblePorCalendario = $menuElemento['disponibilidadForzar'];
            } else {
                // La entrada no tiene definido el despliegue en menú de manera forzada, obedece al calendario de disponibilidad

                // Obtiene la disponibilidad de acuerdo al Calendario programado
                $configEtapa = self::obtenerDisponibilidadModuloPorCalendario($menuElemento['etiqueta'], $menuElemento['calendarioDisponibilidad'], $ahora);

                // Determina si existe disponibilidad
                if ($configEtapa['disponible']) {
                    $disponiblePorCalendario = true;
                }
            }

            // Determina la disponibilidad de acuerdo a calendario
            if (!$disponiblePorCalendario) {
                // La vista solicitada no esta disponible por calendario
                throw new InterfazDisponibleException(('La página solicitada no se encuentra disponible por el momento.') . (($menuElemento['mostrarCalendarioDisponibilidad'] && isset($configEtapa) && !empty($configEtapa['textoCalendario'])) ? '<br>Consultar durante las siguientes fechas:' . $configEtapa['textoCalendario'] : ''));
            }

            // Inicializa la bandera de disponibilidad por usuario
            $disponiblePorUsuario = self::obtenerDisponibilidadModuloPorListaUsuario($primerElemento, $menuElemento['listaBlancaUsuario'], $nombreUsuario);

            // Determina la disponibilidad por usuario
            if (!$disponiblePorUsuario) {
                // La vista solicitada no esta disponible por calendario
                throw new InterfazDisponibleException('La página solicitada no se encuentra disponible para esta sesión.');
            }

            // Construye un nuevo arreglo que define la vista de la vista solicitada, eliminando su primer elemento
            $arrayVistaSolicitadaElemento = array_slice($vistaArreglo, 1);

            // Recupera o inicializa (vacio) la configuración del Elemento del Menú
            $configEtapa = (isset($configEtapa)) ? $configEtapa : array();
            $configElemento = (array_key_exists('config', $menuElemento)) ? array_merge($menuElemento['config'], $configEtapa) : $configEtapa;

            // Limpia las variables auxiliares posibles
            unset($configEtapa);
            unset($enListaBlanca);
            unset($disponiblePorCalendario);
            unset($disponiblePorUsuario);

            // Determina si la vista solicitada tiene subdirectorios
            if (count($arrayVistaSolicitadaElemento) > 0) {
                // Cuando la vista solicitada tiene subdirectorios, realiza la acción recursivamente para unir las configuraciones
                return array_merge($configElemento, self::recorrerElementoMenu($arrayVistaSolicitadaElemento, $menuElemento['hijos'], $ahora, $nombreUsuario));
            } else {
                // La vista solicitada no tiene subdirectorios, ha llegado al fin de la rama

                // Determina si el elemento final tiene una definicion de configuración
                if (array_key_exists('config', $menuElemento)) {
                    return $menuElemento['config'];
                } else {
                    // La vista solicitada no tiene una definicion de configuración
                    throw new ConfigException('La página solicitada no tiene definida una forma de comportamiento.');
                }
            }
        } else {
            // La vista solicitada no ha sido definida como parte de la aplicación
            throw new InterfazDisponibleException('La página solicitada no se ha definido como parte de la aplicación.');
        }
    }

    /**
     * Función generarMenu
     *
     * Genera el menú adecuado para la interfaz.
     *
     * Varía en función de:
     *     - Zona donde esta ubicado el usuario.
     *     - Sesión de usuario.
     *
     * @param array $menu Definicion del menú que debe ser utilizado.
     * @param datetime $ahora Fecha y hora del sistema.
     * @param object $usuarioModelo Objeto que representa un Usuario
     * @param string $nivelAcceso Cadena que representa el nivel de acceso solicitado (Primer subdirectorio)
     *
     * @return string Regresa la cadena que contiene la definicion del menú de la interfaz
     *
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     */
    public static function generarMenu($menu, $ahora, $usuarioModelo, $nivelAcceso)
    {
        // Inicializa el menú
        $menuHTML = '';

        // Determina si existe un menú asociado para la zona
        if (!empty($menu)) {
            // Determina si menu es un Array
            if (!is_array($menu)) {
                throw new ConfigException('El menú de la interfaz no se encuentra definido correctamente.');
            } else {
                // Genera el menú de acuerdo al estado de la sesión
                if (empty($usuarioModelo->perfil) || $usuarioModelo->perfil != $nivelAcceso) {
                    // No se ha seleccionado perfil de usuario o el perfil de usuario no se encuenta en el primer elemento de la vista solicitada
                    if (MODO_MENU_NO_DISPONIBLE) {
                        $menuHTML = self::generarEntradasMenuNoDisponible(null, $menu['hijos'], $ahora);
                    } else {
                        $menuHTML = self::generarEntradasMenu(null, $menu['hijos'], $ahora);
                    }
                } else {
                    // Se ha seleccionado un perfil de usuario y se encuentra en su nivel de acceso
                    if (MODO_MENU_NO_DISPONIBLE) {
                        $menuHTML = self::generarEntradasMenuNoDisponible(null, $menu['hijos'], $ahora, $usuarioModelo->perfil, $usuarioModelo->username);
                    } else {
                        $menuHTML = self::generarEntradasMenu(null, $menu['hijos'], $ahora, $usuarioModelo->perfil, $usuarioModelo->username);
                    }
                }
            }
        }

        // Determina si ha sido posible la generación del menú asociado para la zona
        if (!empty($menuHTML)) {
            $menuHTML = <<<EOF
                <nav class="main-nav barraMenu" role="navigation">
                    <div class="container">
                        <input id="main-menu-state" type="checkbox">
                        <label class="main-menu-btn" for="main-menu-state">
                            <span class="main-menu-btn-icon"></span>
                        </label>
                        <ul class="sm sm-custom submenu" id="main-menu" style="list-style: none;">
                            $menuHTML
                        </ul>
                    </div>
                </nav>
EOF;
        }

        return $menuHTML;
    }

    /**
     * Función generarEntradasMenu
     *
     * Genera las entradas correspondientes al menú asociado.
     *
     * @param string $nivelEntradaMenu Nivel de profundidad de la entrada del menú.
     * @param array $menu Definición del menú asociado a la interfaz
     * @param datetime $ahora Fecha y hora del sistema.
     * @param string $perfil (Opcional) Perfil del usuario que utiliza la aplicación.
     * @param string $nombreUsuario (Opcional) Nombre del usuario que utiliza la aplicación.
     *
     * @return  string Regresa la cadena que contiene la definicion de las entradas del menú de la interfaz
     *
     * @throws Exception Excepcióm por manipulación de fechas
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     */
    private static function generarEntradasMenu($nivelEntradaMenu, $menu, $ahora, $perfil = null, $nombreUsuario = null)
    {
        // Inicializa el menú
        $menuHTML = '';

        // Generando los elementos de primer nivel
        foreach ($menu as $keyElemento => $elemento) {
            // Recupera el nivel de la entrada del menú
            $nivelElemento = isset($elemento['uri']) ? $elemento['uri'] : (empty($nivelEntradaMenu) ? $nivelEntradaMenu : $nivelEntradaMenu . '/') . (empty($keyElemento) ? '' : $keyElemento);

            // Paso 1: Validación por Lista Blanca de Dirección IP

            // Inicializa la bandera de disponibilidad por dirección IP de acceso
            $mostrarElemento = self::obtenerDisponibilidadModuloPorListaIP($elemento['etiqueta'], $elemento['listaBlancaIP']);

            // Determina si debe continuar con la validación para el despliegue en el Menú
            if ($mostrarElemento) {
                // Paso 2: Validación por bandera que define si se trata de un elemento para el Menú

                // Determina si la entrada de menu tiene definida la etiqueta para forzar Mostrar en Menú
                if (array_key_exists('mostrarEnMenuForzar', $elemento)) {
                    // Paso 2.1: Despliegue forzado

                    // La entrada tiene definido el despliegue en menú de manera forzada, obedece dicho valor sin importar el calendario de disponibilidad
                    $mostrarElemento = $elemento['mostrarEnMenuForzar'];
                } else {
                    // Paso 2.2.1: Validación por etiqueta que define si se trata de un elemento de Menú

                    // Determina si la entrada de menu tiene definida la etiqueta que indica si es un elemento del Menú
                    $mostrarElemento = ($elemento['elementoMenu'] === false) ? false : true;

                    // Determina si debe continuar con la validación para el despliegue en el Menú
                    if ($mostrarElemento) {
                        // Paso 2.2.2: Validación por calendario de disponibilidad

                        // Obtiene la disponibilidad de acuerdo al Calendario programado
                        $configEtapa = self::obtenerDisponibilidadModuloPorCalendario($elemento['etiqueta'], $elemento['calendarioDisponibilidad'], $ahora);

                        // Determina si existe disponibilidad
                        $mostrarElemento = ($configEtapa['disponible']) ? true : false;

                        // Determina si debe continuar con la validación para el despliegue en el Menú
                        if ($mostrarElemento) {
                            // Paso 2.2.3: Validación por lista blanca de usuario

                            // Obtiene la disponibilidad de acuerdo al Calendario programado
                            $mostrarElemento = self::obtenerDisponibilidadModuloPorListaUsuario($elemento['etiqueta'], $elemento['listaBlancaUsuario'], $nombreUsuario);
                        }
                    }
                }
            }

            // Determina si debe continuar con la Generación del elemento del Menú
            if ($mostrarElemento) {
                // Paso 3: Generación de entrada

                // Resetea los hijos de la entrada de menú
                $submenuHTML = '';

                // Determina si la entrada de menú tiene elementos hijos
                if (array_key_exists('hijos', $elemento)) {
                    // Genera los hijos recursivamente hasta llegar al final
                    $hijosHTML = self::generarEntradasMenu($nivelElemento, $elemento['hijos'], $ahora, $perfil, $nombreUsuario);

                    // Determina si el elemento debe ser mostrado
                    if (!empty($hijosHTML)) {
                        // El elemento tiene contenido visible, por lo tanto, debe ser mostrado
                        $submenuHTML = '<ul class="submenu" style="list-style: none;">' . $hijosHTML . '</ul>';

                        // Establece la bandera
                        $mostrarElemento = true;
                    } else if (array_key_exists('accion', $elemento['hijos']) || $elemento['mostrarEnMenuForzar']) {
                        // El elemento no tiene contenido visible pero su hijo es una acción (para proceso en BackGround de Web Service), por lo tanto, debe ser mostrado
                        $mostrarElemento = true;
                    } else {
                        // El elemento no tiene contenido visible, por lo tanto, no debe ser mostrado
                        $mostrarElemento = false;
                    }
                }

                // Determina si el elemento debe ser mostrado
                if ($mostrarElemento) {
                    $tooltip = (isset($elemento['tooltip']) && !empty($elemento['tooltip'])) ? 'data-toggle="tooltip-menu" data-placement="top" title="' . $elemento['tooltip'] . '"' : '';
                    $onClick = ($elemento['deshabilitado']) ? "onClick='return false;'" : '';
                    $target = ($elemento['target'] === '') ? '_self' : $elemento['target'];
                    $destino = (preg_match('/^(http|https):\/\/(.)*/i', $nivelElemento)) ? $nivelElemento : RUTA_APLICACION . ($perfil != '' ? $perfil . '/' : '') . $nivelElemento;
                    $menuHTML .= '<li>';
                    $menuHTML .= '<a href="' . $destino . '" target="' . $target . '" ' . $onClick . ' ' . $tooltip . '>' . $elemento['etiqueta'] . '</a>';
                    $menuHTML .= $submenuHTML;
                    $menuHTML .= '</li>';
                }
            }
        }

        return $menuHTML;
    }

    /**
     * Función generarEntradasMenuNoDisponible
     *
     * Genera las entradas correspondientes al menú asociado.
     *
     * @param string $nivelEntradaMenu Nivel de profundidad de la entrada del menú.
     * @param array $menu Definición del menú asociado a la interfaz
     * @param datetime $ahora Fecha y hora del sistema.
     * @param string $perfil (Opcional) Perfil del usuario que utiliza la aplicación.
     * @param string $nombreUsuario (Opcional) Nombre del usuario que utiliza la aplicación.
     *
     * @return string Regresa la cadena que contiene la definicion de las entradas del menú de la interfaz
     *
     * @throws Exception Excepcióm por manipulación de fechas
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     */
    private static function generarEntradasMenuNoDisponible($nivelEntradaMenu, $menu, $ahora, $perfil = null, $nombreUsuario = null)
    {
        // Inicializa el menú
        $menuHTML = '';

        // Generando los elementos de primer nivel
        foreach ($menu as $keyElemento => $elemento) {
            // Recupera el nivel de la entrada del menú
            $nivelElemento = isset($elemento['uri']) ? $elemento['uri'] : (empty($nivelEntradaMenu) ? $nivelEntradaMenu : $nivelEntradaMenu . '/') . (empty($keyElemento) ? '' : $keyElemento);

            // Inicializa la disponibilidad de la entrada
            $configEtapa['disponible'] = false;

            // Paso 1: Validación por Lista Blanca de Dirección IP

            // Inicializa la bandera de disponibilidad por dirección IP de acceso
            $mostrarElemento = self::obtenerDisponibilidadModuloPorListaIP($elemento['etiqueta'], $elemento['listaBlancaIP']);

            // Determina si debe continuar con la validación para el despliegue en el Menú
            if ($mostrarElemento) {
                // Paso 2: Validación por bandera que define si se trata de un elemento para el Menú

                // Determina si la entrada de menu tiene definida la etiqueta que indica si es un elemento del Menú
                $mostrarElemento = ($elemento['elementoMenu'] === false) ? false : true;

                // Determina si debe continuar con la validación para el despliegue en el Menú
                if ($mostrarElemento) {
                    // Paso 2.1: Validación por calendario de disponibilidad

                    // Obtiene la disponibilidad de acuerdo al Calendario programado
                    $configEtapa = self::obtenerDisponibilidadModuloPorCalendario($elemento['etiqueta'], $elemento['calendarioDisponibilidad'], $ahora);

                    // Paso 2.2: Validación por usuario de acceso

                    // Obtiene la disponibilidad por usuario
                    $mostrarElemento = self::obtenerDisponibilidadModuloPorListaUsuario($elemento['etiqueta'], $elemento['listaBlancaUsuario'], $nombreUsuario);
                }
            }

            // Determina si debe continuar con la Generación del elemento del Menú
            if ($mostrarElemento) {
                // Paso 3: Generación de entrada

                // Resetea los hijos de la entrada de menú
                $submenuHTML = '';

                // Determina si la entrada de menú tiene elementos hijos
                if (array_key_exists('hijos', $elemento)) {
                    // Genera los hijos recursivamente hasta llegar al final
                    $hijosHTML = self::generarEntradasMenuNoDisponible($nivelElemento, $elemento['hijos'], $ahora, $perfil, $nombreUsuario);

                    // Determina si el elemento debe ser mostrado
                    if (!empty($hijosHTML)) {
                        // El elemento tiene contenido visible, por lo tanto, debe ser mostrado
                        $submenuHTML = '<ul class="submenu" style="list-style: none;">' . $hijosHTML . '</ul>';

                        // Establece la bandera
                        $mostrarElemento = true;
                    } else if (array_key_exists('accion', $elemento['hijos'])) {
                        // El elemento no tiene contenido visible pero su hijo es una acción (para proceso en BackGround de Web Service), por lo tanto, debe ser mostrado
                        $mostrarElemento = true;
                    } else {
                        // El elemento no tiene contenido visible, por lo tanto, no debe ser mostrado
                        $mostrarElemento = false;
                    }
                }

                // Determina si el elemento debe ser mostrado
                if ($mostrarElemento) {
                    $tooltip = (isset($elemento['tooltip']) && !empty($elemento['tooltip'])) ? 'data-toggle="tooltip-menu" data-placement="top" title="' . $elemento['tooltip'] . '"' : '';
                    $onClick = ($elemento['deshabilitado']) ? "onClick='return false;'" : '';
                    $target = ($elemento['target'] === '') ? '_self' : $elemento['target'];
                    $destino = (preg_match('/^(http|https):\/\/(.)*/i', $nivelElemento)) ? $nivelElemento : RUTA_APLICACION . ($perfil != '' ? $perfil . '/' : '') . $nivelElemento;
                    $menuHTML .= '<li>';
                    if ($configEtapa['disponible']) {
                        $menuHTML .= '<a href="' . $destino . '" target="' . $target . '" ' . $onClick . ' ' . $tooltip . '>' . $elemento['etiqueta'] . '</a>';
                    } else {
                        $menuHTML .= '<a href="#" style="color: gray; pointer-events: none">' . $elemento['etiqueta'] . '<br>(No disponible)</a>';
                    }
                    $menuHTML .= $submenuHTML;
                    $menuHTML .= '</li>';
                }
            }
        }

        return $menuHTML;
    }

    /**
     * Función recuperarEntradasLineaProceso
     *
     * Genera la barra de sesión de usuario.
     *
     * Varía en función de:
     *     - El usuario no ha iniciado sesión.
     *         - En la zona pública indica que puede iniciar sesión.
     *     - El usuario tiene una sesión iniciada.
     *         - En la zona pública indica que puede ingresar a la zona privada.
     *         - En la zona privada indica que puede cerrar sesión.
     *
     * @param array $menu Definición del menú asociado a la interfaz
     * @param string $perfil Perfil de usuario para la linea de proceso
     * @param string $nivelEntrada Nivel de las entradas por recuperar
     *
     * @return array Lista de entradas para la Linea de Proceso
     */
    public static function recuperarEntradasLineaProceso($menu, $perfil, $nivelEntrada)
    {
        // Recupera las entradas dependientes directamente
        $menu = $menu['hijos'];

        // Recupera las entradas dependientes de acuerdo con el nivel de profundidad solicitado
        foreach (explode('/', $nivelEntrada) as $nivel) {
            $menu = $menu[$nivel]['hijos'];
        }

        // Establece la ruta de destino para cada entrada por preparar
        foreach ($menu as $key => $entrada) {
            $menu[$key]['destino'] = RUTA_APLICACION . $perfil . '/' . $nivelEntrada . '/' . $key;
        }

        return $menu;
    }

    /**
     * Función prepararEntradaLineaProceso
     *
     * Obtiene la disponibilidad de un Módulo en funcion de su Calendario de Disponbilidad.
     *
     * @param string $nombreModulo Cadena que representa el nombre del Módulo por validar
     * @param array $calendarioDisponibilidad Arreglo que contiene el Calendario de Disponibilidad
     * @param datetime $ahora Fecha del sistema
     * @param string $formatoFecha (Opcional) Formato de la fecha definida para la aplicación a desarrollar
     * @param string $formatoHora (Opcional) Formato de la hora definida para la aplicación a desarrollar
     * @param string $mascara (Opcional) Orden de despliegue de la cadena resultante
     * @param bool $mostrarProrroga (Opcional) Bandera que indica si el proceso debe mostrar las fechas de prórroga
     *
     * @return array Arreglo con los datos de la entrada del proceso
     *
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     * @throws Exception Excepción por manipulación de fechas
     */
    public static function prepararEntradaLineaProceso($nombreModulo, $calendarioDisponibilidad, $ahora, $formatoFecha = FORMATO_FECHA_FRONTEND, $formatoHora = FORMATO_HORA_FRONTEND, $mascara = 'FECHA, HORA horas', $mostrarProrroga = true)
    {
        // Inicializa los datos para la etapa en la que se encuentra
        $datosPeriodo = array();

        if (empty($calendarioDisponibilidad)) {
            // Establece la bandera que indica que el módulo si esta disponible
            $datosPeriodo['disponible'] = true;
        } else {
            // Determina si el calendario de fechas es un arreglo
            if (!is_array($calendarioDisponibilidad)) {
                throw new ConfigException('El calendario de disponibilidad para el módulo ' . $nombreModulo . ' no se encuentra definido correctamente.');
            }

            // Recorre cada una de las fechas de disponibilidad para determinar si la interfaz debe estar activa
            foreach ($calendarioDisponibilidad as $periodo) {
                // Recupera la hora de inicio y fin indicada

                $inicio = self::obtenerFechaPeriodoGracia($periodo['fecha_inicio'], 'APERTURA', $periodo['periodo_gracia_apertura']);
                $fin = self::obtenerFechaPeriodoGracia($periodo['fecha_fin'], 'CIERRE', $periodo['periodo_gracia_cierre']);

                $inicioTexto = self::convertirFechaHoraCadena(new DateTime($periodo['fecha_inicio']), $formatoFecha, $formatoHora, $mascara);
                $finTexto = self::convertirFechaHoraCadena(new DateTime($periodo['fecha_fin']), $formatoFecha, $formatoHora, $mascara);

                // Determina si la fecha de inicio es igual a la fecha de fin, en ese caso, debe agregar horas de siponibilidad
                if ($inicioTexto == $finTexto) {
                    $textoFecha = $inicioTexto . ', ' . AppHerramientas::convertirFechaHoraCadena(new DateTime($periodo['fecha_inicio']), $formatoFecha, $formatoHora, 'HORA') . ' a ' . AppHerramientas::convertirFechaHoraCadena(new DateTime($periodo['fecha_fin']), $formatoFecha, $formatoHora, 'HORA') . ' horas';
                } else {
                    $textoFecha = $inicioTexto . ' a ' . $finTexto;
                }

                // Determina si debe sobrescribir el nombre del periodo
                if ($periodo['esProrroga']) {
                    $periodo['nombrePeriodo'] = 'prórroga';
                }

                // Determina si el periodo debe ser mostrado en el texto
                // - NO es prórroga
                // - SI es prórroga y debe ser mostrada
                if ($periodo['esProrroga'] !== true || ($periodo['esProrroga'] && $mostrarProrroga)) {
                    // Ajusta la cadena de calendario de disponibilidad
                    $datosPeriodo['textoCalendario'] .= empty($datosPeriodo['textoCalendario']) ? '' : '<br>';
                    $datosPeriodo['textoCalendario'] .= $textoFecha;

                    // Ajuste y validación cuando el Nombre del Periodo debe ser mostrado
                    if ($periodo['mostrarPeriodo']) {
                        if (empty($periodo['nombrePeriodo'])) {
                            throw new ConfigException('El nombre del periodo para el módulo ' . $nombreModulo . ' no ha sido definido correctamente.');
                        }

                        // Concatena el nombre del periodo al calendario de disponibilidad
                        $datosPeriodo['textoCalendario'] .= ' (' . $periodo['nombrePeriodo'] . ')';
                    }
                }

                // Determina si la fecha y hora actual se encuentra entre los rangos de tiempo deifnidos
                if ($ahora >= $inicio && $ahora <= $fin) {
                    // Establece la bandera que indica que el módulo si esta disponible
                    $datosPeriodo['disponible'] = true;

                    // Establece la etapa en la que se encuentra
                    $datosPeriodo['etapa'] = $periodo['nombrePeriodo'];
                }
            }
        }

        return $datosPeriodo;
    }

    /**
     * Función generarBarraUsuario
     *
     * Genera la barra de sesión de usuario.
     *
     * Varía en función de:
     *     - El usuario no ha iniciado sesión.
     *         - En la zona pública indica que puede iniciar sesión.
     *     - El usuario tiene una sesión iniciada.
     *         - En la zona pública indica que puede ingresar a la zona privada.
     *         - En la zona privada indica que puede cerrar sesión.
     *
     * @param array $vistaArreglo Vista solicitada por el usuario.
     * @param boolean $sesionIniciada Indica la existencia de una sesión de usuario
     * @param object $usuarioModelo Objeto que representa un Usuario
     * @param string $nivelAcceso Cadena que representa el nivel de acceso solicitado (Primer subdirectorio)
     * @param boolean $zonaPrivada Indica si el nivel de acceso soliictado se encentra en la Zona Privada
     *
     * @return string Regresa la cadena que contiene la definición de la barra de usuario
     *
     * @throws ConfigException Excepción por error de configuración
     */
    public static function generarBarraUsuario($vistaArreglo, $sesionIniciada, $usuarioModelo, $nivelAcceso, $zonaPrivada)
    {
        // Inicializa la barra de usuario
        $barraUsuario = '';
        $barraAjustesUsuario = null;

        // Determina si existe una sesión activa de usuario
        if ($sesionIniciada) {
            // Existe una sesión de usuario

            // Determina la zona en la que se encuentra el usuario
            if ($zonaPrivada) {
                // Se encuentra en la zona privada

                // Ajusta las etiquetas para la barra de uruario
                $href = 'logout';
                $label = 'Cerrar Sesión';

                // Recupera las opciones del Perfil
                $opciones = AppHerramientas::obtenerDatosPerfil($usuarioModelo->perfil)['opcionAjuste'];

                // Construye las opciones
                $ajustesUsuario = '';
                if (is_array($opciones)) {
                    foreach ($opciones as $opcion) {
                        $ajustesUsuario .= '<a class="dropdown-item" href="' . RUTA_APLICACION . $usuarioModelo->perfil . '/' . $opcion['uri'] . '">' . $opcion['label'] . '</a>';
                    }
                }

                // Si el usuario tiene mas de un perfil asignado, genera la opción de cambiar de perfil de usuario
                $ajustesUsuario .= (count($usuarioModelo->listaPerfil) > 1) ? '<a class="dropdown-item" href="' . RUTA_APLICACION . 'elegirPerfil">Cambiar perfil</a>' : '';

                // Genera la seccion de "ajustes" para el usuario logueado
                if (!empty($ajustesUsuario)) {
                    $barraAjustesUsuario = <<<EOF
                        <li>
                            <div class="p-1" data-toggle="dropdown" style="cursor: pointer" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-cog fa-lg mr-2 mt-1 text-danger"></i>
                            </div>
                            <div class="dropdown-menu">
                                $ajustesUsuario
                            </div>
                        </li>
EOF;
                } else {
                    $barraAjustesUsuario = '';
                }
            } else {
                // Se encuentra en la zona pública

                // Determina si el usuario cuenta con perfil de acceso elegido
                if (empty($usuarioModelo->perfil)) {
                    // El usuario aún no ha elegido un perfil de usuario

                    // Determina la interfaz de la zona pública en donde se encuentra
                    if (count($vistaArreglo) == 1 && $nivelAcceso == 'elegirPerfil') {
                        // El usuario esta en la interfaz para elegir perfil de usuario, el atributo HREF dirige a la interfaz de cerrar sesión
                        $href = 'logout';
                        $label = 'Cerrar Sesión';
                    } elseif (count($vistaArreglo) == 2 && $vistaArreglo[0] == 'cuenta' && ($vistaArreglo[1] == 'activar' || $vistaArreglo[1] == 'restaurar')) {
                        // El usuario esta en la interfaz para activar o restaurar la cuenta de usuario, el atributo HREF dirige a la interfaz de cerrar sesión
                        $href = 'logout';
                        $label = 'Cerrar Sesión';
                    } else {
                        // El usuario esta navegando por la zona pública, el atributo HREF dirige a la interfaz para elegir un perfil de usuario
                        $href = 'elegirPerfil';
                        $label = 'Acceder';
                    }
                } else {
                    // El usuario ya ha elegido un perfil de usuario

                    // El atributo HREF dirige al subirectorio apropiado de acuerdo al perfil de usuario
                    $href = $usuarioModelo->perfil . '/';
                    $label = 'Regresar a ' . self::obtenerDatosPerfil($usuarioModelo->perfil)['nombre'];
                }
            }
        } else {
            // No existe una sesión de usuario.

            // Determina si la aplicación requiere del botón de inicio de sesión.
            if (INICIAR_SESION) {
                // La aplicación requiere del botón de iniciar sesión.

                // Ajusta los atributos.
                // El atributo HREF dirige al formulario de inicio de sesión.
                $href = 'iniciarSesion';
                $label = 'Iniciar Sesión';
            }
        }

        // Determina se existen elementos por mostrar en la barra de usuario
        if (!empty($href) && !empty($label)) {
            $href = RUTA_APLICACION . $href;
            // Genera la barra de usuario
            $barraUsuario = <<<EOF
                    <div class="container pt-2 barraUsuario">
                        <div class="row flex-nowrap justify-content-end align-items-center">
                            <div class="col-12 d-flex justify-content-end align-items-center">
                                <ul class="nav nav-pills" style="list-style: none;">
                                    $barraAjustesUsuario
                                    <li>
                                        <i class="fas fa-user fa-lg mr-2 mt-2 text-danger"></i>
                                    </li>
                                    <li class="nav-item">
                                        <div class="mr-2 p-1"><strong>$usuarioModelo->username</strong></div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="btn btn-sm btn-outline-danger" href="$href">$label</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
EOF;
        }

        return $barraUsuario;
    }

    /**
     * Función convertirFechaCadena
     *
     * Convierte una fecha en formato YYYY-MM-DD a un formato predeterminado (DD de MMMM de YYYYY)
     *
     * @param datetime $fecha Fecha del sistema
     * @param string $formato (Opcional) Formato para presentar la fecha
     *
     * @return string Regresa una cadena formateada
     */
    public static function convertirFechaCadena($fecha, $formato = FORMATO_FECHA_FRONTEND)
    {
        // Determina el mes de la fecha indicada
        switch ($fecha->format('m')) {
            case 1:
                $mes = 'enero';
                break;
            case 2:
                $mes = 'febrero';
                break;
            case 3:
                $mes = 'marzo';
                break;
            case 4:
                $mes = 'abril';
                break;
            case 5:
                $mes = 'mayo';
                break;
            case 6:
                $mes = 'junio';
                break;
            case 7:
                $mes = 'julio';
                break;
            case 8:
                $mes = 'agosto';
                break;
            case 9:
                $mes = 'septiembre';
                break;
            case 10:
                $mes = 'octubre';
                break;
            case 11:
                $mes = 'noviembre';
                break;
            case 12:
                $mes = 'diciembre';
                break;
            default:
                $mes = 'desconocido';
                break;
        }

        $fechaString = str_replace('DD', $fecha->format('d'), $formato);
        $fechaString = str_replace('MM', $mes, $fechaString);
        $fechaString = str_replace('mm', $fecha->format('m'), $fechaString);
        $fechaString = str_replace('YYYY', $fecha->format('Y'), $fechaString);
        $fechaString = str_replace('YY', $fecha->format('y'), $fechaString);

        return $fechaString;
    }

    /**
     * Función convertirFechaHoraCadena
     *
     * Convierte una fecha en formato YYYY-MM-DD y una hora en formato hh:mm a una cadena de texto.
     *
     * @param datetime $fecha Fecha por convertir
     * @param string $formatoFecha (Opcional) Formato de la fecha definida para la aplicación a desarrollar
     * @param string $formatoHora (Opcional) Formato de la hora definida para la aplicación a desarrollar
     * @param string $mascara (Opcional) Orden de despliegue de la cadena resultante
     *
     * @return string Regresa una cadena formateada
     */
    public static function convertirFechaHoraCadena($fecha, $formatoFecha = FORMATO_FECHA_FRONTEND, $formatoHora = FORMATO_HORA_FRONTEND, $mascara = 'FECHA, HORA horas')
    {
        // Formatea la fecha
        $fechaString = self::convertirFechaCadena($fecha, $formatoFecha);
        $fechaHora = str_replace('FECHA', $fechaString, $mascara);

        // Determina si debe agregar la hora
        if (!is_null($formatoHora)) {
            $horaString = $fecha->format($formatoHora);
            $fechaHora = str_replace('HORA', $horaString, $fechaHora);
        }

        return trim($fechaHora);
    }

    /**
     * Función validarUsuario
     *
     * Valida la estructura de un nombre de usuario.
     *
     * @param string $usuario Nombre de usuario.
     *
     * @throws InputDataException Excepción cuando existe una falla por los datos de entrada
     */
    public static function validarUsuario($usuario)
    {
        if (!preg_match(EXPRESION_REGULAR_USUARIO, $usuario)) {
            throw new InputDataException('El nombre de usuario no cumple con el formato correcto.');
        }
    }

    /**
     * Función validarPassword
     *
     * Valida la estructura de una contraseña de acceso.
     *
     * @param string $password Contraseña de acceso.
     * @param string $tipo (Opcional) Tipo de contraseña.
     *
     * @throws InputDataException Excepción cuando existe una falla por los datos de entrada
     */
    public static function validarPassword($password, $tipo = null)
    {
        if (!preg_match(EXPRESION_REGULAR_PASSWORD, $password)) {
            $tipo = (!is_null($tipo)) ? ' (' . $tipo . ') ' : ' ';
            throw new InputDataException('La contraseña' . $tipo . 'no cumple con el formato correcto.');
        }
    }

    /**
     * Función validarFechaNacimientoPassword
     *
     * Valida la estructura de una contraseña de acceso (fecha de nacimiento).
     *
     * @param string $password Contraseña de acceso (fecha de nacimiento).
     *
     * @throws  InputDataException Excepción cuando existe una falla por los datos de entrada
     */
    public static function validarFechaNacimientoPassword($password)
    {
        // Determina longitud y tipo de datos de entrada
        if (!preg_match("/^([0-9]{8})$/", $password)) {
            throw new InputDataException('La fecha de nacimiento no cumple con el formato adecuado.');
        }
    }

    /**
     * Función validarCorreoElectronico
     *
     * Determina si se recibio un correo electrónico.
     * Determina si el correo electrónico recibido cumple con el formato adecuado.
     *
     * @param string $cadena Cadena por validar.
     * @param string $campo (Opcional) Nombre del campo por validar.
     *
     * @throws InputDataException Excepción cuando la cadena no cumple con el formato adecuado.
     */
    public static function validarCorreoElectronico($cadena, $campo = 'correo electrónico')
    {
        self::validarVacio($cadena, $campo);
        if (!preg_match("(^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$)", $cadena)) {
            throw new InputDataException('El campo <strong>' . $campo . '</strong> no se reconoce como un formato adecuado.');
        }
    }

    /**
     * Función validarSeguridadPassword
     *
     * Determina si la contraseña cumple con los parámetros de seguridad establecidos.
     *
     * @param string $password Contraseña por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarSeguridadPassword($password)
    {
        // Inicializa lista de observaciones
        $listaObservacion = array();

        // Recupera la longitud de la contraseña
        $longitudPassword = strlen($password);

        if ($longitudPassword < 6) {
            // Valida longitud de contraseña
            array_push($listaObservacion, 'Mínimo 6 caracteres');
        } elseif ($longitudPassword > 16) {
            // Valida longitud de contraseña
            array_push($listaObservacion, 'Máximo 16 caracteres');
        } elseif (!preg_match(EXPRESION_REGULAR_PASSWORD, $password)) {
            // Valida caracteres permitidos
            array_push($listaObservacion, 'Sólo se admiten los caracteres: <ul><li>A - Z (mayúsculas y minúsculas)</li><li>0 - 9</li><li>Especiales <strong>-</strong>, <strong>!</strong>, <strong>?</strong>, <strong>&</strong>, <strong>/</strong>, <strong>(</strong>, <strong>)</strong> y <strong>#</strong></li></ul>');
        } else {
            // Valida existencia mínima de caracteres
            if (!preg_match('/[A-Z]+/', $password)) {
                array_push($listaObservacion, 'Debe contener al menos una mayúscula');
            }
            if (!preg_match('/[a-z]+/', $password)) {
                array_push($listaObservacion, 'Debe contener al menos una minúscula');
            }
            if (!preg_match('/[0-9]+/', $password)) {
                array_push($listaObservacion, 'Debe contener al menos una número');
            }
            if (!preg_match('/[\-\!\?\&\/\(\)\#]+/', $password)) {
                array_push($listaObservacion, 'Debe contener al menos un caracter especial (<strong>-</strong>, <strong>!</strong>, <strong>?</strong>, <strong>&</strong>, <strong>/</strong>, <strong>(</strong>, <strong>)</strong> o <strong>#</strong>)');
            }

            // Valida caracteres únicos mínimos
            if (strlen(count_chars($password, 3)) < 6) {
                array_push($listaObservacion, 'Debe contener al menos 6 caracteres diferentes');
            }
        }

        // Determina si existe algún mensaje para el usuario
        if (count($listaObservacion) > 0) {
            // Inicializa mensaje
            $mensajeObservacion = '';

            // Recorre cada observación
            foreach ($listaObservacion as $observacion) {
                $mensajeObservacion .= '<li>' . $observacion . '</li>';
            }

            // Despliega mensaje para el usuario
            throw new InputDataException('La contraseña no cumple con los requisitos:<ul>' . $mensajeObservacion . '</ul>');
        }
    }

    /**
     * Función obtenerMascaraCorreo
     *
     * Enmascara una dirección de correo electrónico.
     *
     * @param string $correo Dirección de correo electrónico.
     *
     * @return string Dirección de correo electrónico enmascarada
     */
    public static function obtenerMascaraCorreo($correo)
    {
        // Reemplaza el usuario de la dirección de correo electrónico por una máscara
        $correoExplode = explode('@', $correo);

        // Recupera el usuario y dominio
        $usuario = $correoExplode[0];
        $dominio = $correoExplode[1];

        // Determina las posiciones de inicio y fin para el reemplazo
        $longitudUsuario = strlen($usuario);
        if ($longitudUsuario <= 2) {
            $inicioReemplazo = 0;
            $finReemplazo = $longitudUsuario - 1;
        } else {
            $inicioReemplazo = 1;
            $finReemplazo = $longitudUsuario - 2;
        }

        // Reemplaza de acuerdo con las posiciones de inicio y fin
        for ($i = $inicioReemplazo; $i <= $finReemplazo; $i++) {
            $usuario = substr_replace($usuario, '*', $i, 1);
        }

        // Limpia variables
        unset($longitudUsuario);
        unset($inicioReemplazo);
        unset($finReemplazo);
        unset($i);

        return $usuario . '@' . $dominio;
    }

    /**
     * Función validarCaptcha
     *
     * Valida la solicitud contra el servicio de Re-Captcha de Google.
     *
     * @param string $captcha Valor recibido del Captcha.
     *
     * @throws InputDataException Excepción cuando existe una falla por los datos de entrada
     * @throws WebServiceException Excepción por indisponibilidad del servicio de Captcha
     * @throws AuthException Exception por porblemas de autenticación (No aplica, las peticicones no llevan Auth)
     */
    public static function validarCaptcha($captcha)
    {
        // En un ambiente de PRODUCCION o RESPALDO debe realizar la validación de ReCaptcha
        if (VALIDAR_CAPTCHA) {
            try {
                // Construye un objeto para realizar la validación del captcha.
                $payloadCaptcha = new stdClass();
                $payloadCaptcha->secret = CAPTCHA_PRIVATE;
                $payloadCaptcha->response = $captcha;
                $payloadCaptcha->remoteip = AppHerramientas::obtenerIPOrigenSolicitud();

                // Crea el objeto para realizar la petición al servicio de validación de captcha.
                $peticionWebServiceModelo = new PeticionWebServiceModelo();
                $peticionWebServiceModelo->uri = SERVICIO_CAPTCHA;
                $peticionWebServiceModelo->payload = $payloadCaptcha;
                $peticionWebServiceModelo->metodo = METODO_POST;
                $peticionWebServiceModelo->contentType = 'application/x-www-form-urlencoded';

                // Realiza la petición al servicio de validación de captcha y recupera su respuesta.
                $respuestaCaptcha = ClienteRest::ejecutar($peticionWebServiceModelo);

                if (VERSION_CAPTCHA == 2) {
                    // Realiza la petición al servicio de validación de captcha y recupera su respuesta.
                    if ($respuestaCaptcha->success != true) {
                        throw new InputDataException('El código de verificación no es correcto.');
                    }
                } elseif (VERSION_CAPTCHA == 3) {
                    if (!$respuestaCaptcha->success || $respuestaCaptcha->score < CAPTCHA_SCORE) {
                        throw new InputDataException('El comportamiento no parece humano, intente nuevamente.');
                    }
                }
            } catch (WebServiceException $swEx) {
                throw new WebServiceException('El servicio de Captcha no esta disponible');
            }
        }
    }

    /**
     * Función validarDisponibilidadAplicacion
     *
     * Determina si la aplicación esta disponible.
     *
     * @param datetime $ahora Fecha y hora del sistema.
     *
     * @throws AppDisponibleException Excepción cuando la aplicación no se encuentra disponible.
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     * @throws Exception Excepción por manipulación de fechas
     */
    public static function validarDisponibilidadAplicacion($ahora)
    {
        // Determina si existe una fecha de inicio para la aplicación
        if (defined('PERIODO_ACTIVO_APERTURA')) {
            $inicio = self::obtenerFechaPeriodoGracia(PERIODO_ACTIVO_APERTURA, 'APERTURA');
        }

        if (defined('PERIODO_ACTIVO_CIERRE')) {
            $fin = self::obtenerFechaPeriodoGracia(PERIODO_ACTIVO_CIERRE, 'CIERRE');
        }

        if (isset($inicio) && isset($fin)) {
            if ($ahora < $inicio || $ahora > $fin) {
                $inicioTexto = self::convertirFechaHoraCadena($inicio);
                $finTexto = self::convertirFechaHoraCadena($fin);
                throw new AppDisponibleException('El sistema no se encuentra disponible en este momento.<br>Sólo esta disponible de las <b>' . $inicioTexto . '</b> a las <b>' . $finTexto . '</b>.');
            }
        } elseif (isset($inicio) && !isset($fin)) {
            throw new ConfigException('No se há definido la fecha en que dejará de estar disponible la aplicación.');
        } elseif (!isset($inicio) && isset($fin)) {
            throw new ConfigException('No se há definido la fecha en que comenzará a estar disponible la aplicación.');
        }
    }

    /**
     * Función consultarWebService
     *
     * Realiza peticiones a un Web Service.
     *
     * @param PeticionWebServiceModelo $peticionWebServiceModelo Propiedades de la petición al Web Service
     *
     * @return object Objeto general que contiene una respuesta de un Web Service
     */
    public static function consultarWebService($peticionWebServiceModelo)
    {
        // Inicializa la respuesta esperada del Web Service
        $respuestaWebService = null;

        try {
            // Realiza la petición al Web Service.
            $respuestaWebService = ClienteRest::ejecutar($peticionWebServiceModelo);

            // Determina si la respuesta del Web Service ha regresado un Token de Acceso
            if (isset($respuestaWebService->accessToken) && !empty($respuestaWebService->accessToken)) {
                // Se recibió un Token de Acceso, se actualiza el dato en la variable de sesión correspondiente
                $_SESSION['accessToken'] = $respuestaWebService->accessToken;
            }
        } catch (InputDataException $idEx) {
            // Captura la excepción por falta o inconsistencia de los datos de entrada.

            // Recupera la excepción en una variable local.
            $excepcion = $idEx->getMessage();
        } catch (AuthException $autEx) {
            // Captura la excepción por falla en la autenticación.

            // Recupera la excepción en una variable local.
            $excepcion = $autEx->getMessage();
        } catch (WebServiceException $wsEx) {
            // Captura la excepción por falla en comunicación con el Web Service.

            // Recupera la excepción en una variable local.
            $excepcion = $wsEx->getMessage();
        } catch (Exception $ex) {
            // Captura la excepción general (un error no clasificado).

            // Recupera la excepción en una variable local.
            $excepcion = (MODO_DEBUG) ? 'Error no clasificado: ' . $ex->getMessage() : 'El servicio no esta disponible temporalmente.';
        }

        // Determina si la variable local de excepción esta vaciá.
        if (!empty($excepcion)) {
            // La variable local no esta vaciá, hay algún error por mostrarle al usuario.

            // Crea un objeto de la Clase MensajeModelo.
            $mensajeModelo = new MensajeModelo(MENSAJE_ERROR, 'Servicio', $excepcion, true);

            // Crea un objeto de la Clase RespuestaWebServiceModelo y establece sus propiedades
            $respuestaWebService = new RespuestaWebServiceModelo();
            $respuestaWebService->estado = 400;
            $respuestaWebService->mensaje = $mensajeModelo;
        }

        return $respuestaWebService;
    }

    /**
     * Función cargarVistaEnVariable
     *
     * Carga el contenido generado por una vista (archivo) en una variable
     *
     * @param string $archivoVista Archivo que define la vista
     * @param object $datos (Opcional) Datos para ser utilizados en la vista
     *
     * @return string Contenido generado por la vista
     *
     * @throws RequireException Excepción por inexistencia de la vista indicada
     */
    public static function cargarVistaEnVariable($archivoVista, $datos = null)
    {
        // Determina si el archivo de la vista ha sido definido y existe
        if (empty($archivoVista) || !file_exists($archivoVista)) {
            throw new RequireException((MODO_DEBUG) ? 'La vista <strong>' . $archivoVista . '</strong> no se encontró en el sistema de archivos.' : 'No fue posible cargar la interfaz.');
        }

        // Genera el buffer para leer el archivo solicitado
        ob_start();

        // Carga el archivo de la vista
        require $archivoVista;

        // Recupera el contenido
        return ob_get_clean();
    }

    /**
     * Función desplegarMensajeInterfaz
     *
     * Carga la vista de mensajes para la interfaz de usuario.
     *
     * @param MensajeModelo $mensajeModelo Detalles del mensaje a mostrar
     *
     * @throws RequireException Excepción por inexistencia de la vista indicada
     */
    public static function desplegarMensajeInterfaz($mensajeModelo = null)
    {
        // Determina si existe mensaje que deba ser desplegado
        if ($mensajeModelo != null && $mensajeModelo->desplegar) {
            // Ajusta los datos por enviar
            $datos = new stdClass();
            $datos->mensajeModelo = $mensajeModelo;
            switch ($mensajeModelo->tipo) {
                case MENSAJE_EXITO:
                    $datos->estiloAlerta = 'alert-success';
                    break;
                case MENSAJE_ADVERTENCIA:
                    $datos->estiloAlerta = 'alert-warning';
                    break;
                case MENSAJE_INFO:
                    $datos->estiloAlerta = 'alert-info';
                    break;
                case MENSAJE_ERROR:
                default:
                    $datos->estiloAlerta = 'alert-danger';
                    break;
            }

            //Imprime el contenido generado
            echo self::cargarVistaEnVariable(__DIR__ . '/../' . CARPETA_VISTA . '/_mensajeInterfaz.php', $datos);

            // Destrute el objeto auxiliar
            unset($datos);
        }
    }

    /**
     * Función validarVacio
     *
     * Determina si el dato recibido se encuentra vacio.
     *
     * @param string $cadena Cadena por validar.
     * @param string $campo Nombre del campo por validar.
     * @param boolean $ceroPermitido (opcional) Bandera que indica si el valor 0 esta permitido.
     *
     * @throws InputDataException Excepción cuando la cadena esta vacia.
     */
    public static function validarVacio($cadena, $campo, $ceroPermitido = false)
    {
        // Determina si la cadena recibida se encuentra vacia
        if ((!$ceroPermitido && empty($cadena)) || ($ceroPermitido && $cadena != '0' && empty($cadena))) {
            throw new InputDataException('Es necesario proporcionar el campo <strong>' . $campo . '</strong>.');
        }
    }

    /**
     * Función convertirMayusculas
     *
     * Convierte una cadena a mayúsculas.
     *
     * @param string $cadena Cadena por limpiar.
     *
     * @return string Cadena resultante
     */
    public static function convertirMayusculas($cadena)
    {
        return mb_strtoupper($cadena, 'UTF-8');
    }

    /**
     * Función obtenerContentTypePorExtension
     *
     * Determina el valor de la propiedad ContentType apropiado para la extensión indicada.
     *
     * @param string $extension Extensión del archivo.
     *
     * @return String ContentType asociado a la extensión
     */
    public static function obtenerContentTypePorExtension($extension)
    {
        // Declara el diccionario
        $diccionarioContentType['pdf'] = 'application/pdf';
        $diccionarioContentType['exe'] = 'application/octet-stream';
        $diccionarioContentType['zip'] = 'application/zip';
        $diccionarioContentType['doc'] = 'application/msword';
        $diccionarioContentType['xls'] = 'application/vnd.ms-excel';
        $diccionarioContentType['ppt'] = 'application/vnd.ms-powerpoint';
        $diccionarioContentType['gif'] = 'image/gif';
        $diccionarioContentType['png'] = 'image/png';
        $diccionarioContentType['jpeg'] = 'image/jpg';
        $diccionarioContentType['jpg'] = 'image/jpg';
        $diccionarioContentType['default'] = 'application/force-download';

        // Recupera el ContentType asociado
        return (array_key_exists($extension, $diccionarioContentType)) ? $diccionarioContentType[$extension] : $diccionarioContentType['default'];
    }

    /**
     * Función descargarArchivo
     *
     * Método genérico para generar la descarga de archivos.
     *
     * @param string $rutaArchivo Ubicación del archivo por descargar.
     * @param string $nombreArchivo Nombre del archivo por descargar.
     *
     * return file Archivo
     */
    public static function descargarArchivo($rutaArchivo, $nombreArchivo)
    {
        // Recupera los parametos necesarios para el archivo indicado
        $extension = pathinfo($rutaArchivo, PATHINFO_EXTENSION);
        $contentType = self::obtenerContentTypePorExtension($extension);

        # Salida del archivo
        header('Content-Type: ' . $contentType);
        header('Content-Disposition: attachment; filename="' . $nombreArchivo . '"');
        header('Content-Length: ' . filesize($rutaArchivo));
        readfile($rutaArchivo);
    }

    /**
     * Función formatearSemestreLectivo
     *
     * Transforma una cadena que define el semestre lectivo del formato de procesamiento al formato de presentación.
     *
     * @param string $semestre Cadena que define el semestre lectivo (formato de procesamiento).
     *
     * @return string Semestre lectivo con formato de presentación
     *
     * @throws InputDataException  Excepción por dato de entrada incorrecto
     */
    public static function formatearSemestreLectivo($semestre)
    {
        // Valida el semestre
        self::validarVacio($semestre, 'Semestre lectivo');
        if (!preg_match('/^([0-9]{5})$/', $semestre)) {
            throw new InputDataException('El semestre lectivo no cumple con el formato correcto.');
        }

        return substr($semestre, 0, strlen($semestre) - 1) . '-' . substr($semestre, -1);
    }

    /**
     * Función obtenerDatosPerfil
     *
     * Recupera los datos asociados al perfil indicado.
     *
     * @param string $perfil Nombre del perfil de usuario.
     *
     * @return array Arreglo con las propiedades del perfil
     *
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     */
    public static function obtenerDatosPerfil($perfil)
    {
        // Construye la ruta del archivo donde se define el diccionario de perfiles
        $archivoPerfil = __DIR__ . '/../../aplicacion/configuracion/AppPerfil.php';

        // Determina si el archivo existe
        if (!file_exists($archivoPerfil)) {
            throw new ConfigException('No se encuentra el archivo que define los perfiles.');
        }

        // Importa el archivo que define el Diccionario de Perfiles
        $diccionarioPerfil = array();
        require $archivoPerfil;

        // Busca el perfil indicado
        if (array_key_exists($perfil, $diccionarioPerfil)) {
            // Recupera el valor deseado para el perfil indicado
            $perfil = $diccionarioPerfil[$perfil];
        } else {
            throw new ConfigException('El perfil indicado (' . $perfil . ') no se encuentra definido.');
        }

        return $perfil;
    }

    /**
     * Función obtenerDatosODBC
     *
     * Recupera los datos asociados al perfil indicado.
     *
     * @param string $perfilODBC Nombre del perfil de Origen de Base de Datos.
     *
     * @return array Arreglo con las propiedades del perfil
     *
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     */
    public static function obtenerDatosODBC($perfilODBC)
    {
        // Construye la ruta del archivo donde se define el diccionario de perfiles
        $archivoODBC = __DIR__ . '/../../aplicacion/configuracion/AppODBC.php';

        // Determina si el archivo existe
        if (!file_exists($archivoODBC)) {
            throw new ConfigException('No se encuentra el archivo que define los Origenes de Base de Datos.');
        }

        // Importa el archivo que define el Diccionario de Perfiles
        $diccionarioODBC = array();
        require $archivoODBC;

        // Busca el perfil indicado
        if (array_key_exists($perfilODBC, $diccionarioODBC)) {
            // Recupera el valor deseado para el perfil indicado
            $perfilODBC = $diccionarioODBC[$perfilODBC];
        } else {
            throw new ConfigException('El perfil indicado (' . $perfilODBC . ') no se encuentra definido.');
        }

        return $perfilODBC;
    }

    /**
     * Función obtenerDatosBD
     *
     * Recupera los datos asociados al perfil indicado.
     *
     * @param string $perfilBD Nombre del perfil de para conexión a la Base de Datos.
     *
     * @return array Arreglo con las propiedades del perfil
     *
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     */
    public static function obtenerDatosBD($perfilBD)
    {
        // Construye la ruta del archivo donde se define el diccionario de perfiles
        $archivoBD = __DIR__ . '/../../aplicacion/configuracion/AppBD.php';

        // Determina si el archivo existe
        if (!file_exists($archivoBD)) {
            throw new ConfigException('No se encuentra el archivo que define los Origenes de Base de Datos.');
        }

        // Importa el archivo que define el Diccionario de Perfiles
        $diccionarioBD = array();
        require $archivoBD;

        // Busca el perfil indicado
        if (array_key_exists($perfilBD, $diccionarioBD)) {
            // Recupera el valor deseado para el perfil indicado
            $perfilBD = $diccionarioBD[$perfilBD];
        } else {
            throw new ConfigException('El perfil indicado (' . $perfilBD . ') no se encuentra definido.');
        }

        return $perfilBD;
    }

    /**
     * Función obtenerDatosMail
     *
     * Recupera los datos asociados al perfil indicado.
     *
     * @param string $perfilMail Nombre del perfil de para conexión al Servidore de Correo.
     *
     * @return array Arreglo con las propiedades del perfil
     *
     * @throws ConfigException Excepción cuando el archivo de configuración no esta definido correctamente
     */
    public static function obtenerDatosMail($perfilMail)
    {
        // Construye la ruta del archivo donde se define el diccionario de perfiles
        $archivoMail = __DIR__ . '/../../aplicacion/configuracion/AppMail.php';

        // Determina si el archivo existe
        if (!file_exists($archivoMail)) {
            throw new ConfigException('No se encuentra el archivo que define la configuración para conexión al Servidor de Correo.');
        }

        // Importa el archivo que define el Diccionario de Perfiles
        $diccionarioMail = array();
        require $archivoMail;

        // Busca el perfil indicado
        if (array_key_exists($perfilMail, $diccionarioMail)) {
            // Recupera el valor deseado para el perfil indicado
            $perfilMail = $diccionarioMail[$perfilMail];
        } else {
            throw new ConfigException('El perfil indicado (' . $perfilMail . ') no se encuentra definido.');
        }

        return $perfilMail;
    }

    /**
     * Función obtenerDatosVisitas
     *
     * Recupera las visitas del sitio.
     */
    public static function obtenerDatosVisitas()
    {
        // Construye la ruta del archivo donde se define el diccionario de perfiles
        $archivoVisitas = __DIR__ . '/../../aplicacion/visitas.txt';

        // Determina si el archivo existe
        if (file_exists($archivoVisitas)) {
            echo '<strong>Visitas: </strong>';
            require $archivoVisitas;
        }
    }

    /**
     * Función obtenerImagenB64
     *
     * Convierte una imagen en Base64.
     *
     * @param string $rutaImagen Ruta de la imagen por convertir
     *
     * @return string Imagen codificada en Base64
     */
    public static function obtenerImagenB64($rutaImagen)
    {
        if (file_exists($rutaImagen)) {
            return $imgB64 = 'data:image/' . pathinfo($rutaImagen, PATHINFO_EXTENSION) . ';base64,' . base64_encode(file_get_contents($rutaImagen));
        } else {
            return $imgB64 = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
        }
    }

    /**
     * Función obtenerHash
     *
     * Obtiene el código Hash de una cadena.
     *
     * @param string $cadena Cadena por obtener su Hash
     *
     * @return string Imagen codificada en Base64
     */
    public static function obtenerHash($cadena)
    {
        return hash(ALGORITMO_HASH_LLAVES, $cadena);
    }

    /**
     * Función obtenerCatalogoMes
     *
     * Genera y entrega un catálogo de los meses.
     *
     * @return   array      Lista que contiene el catálogo de los meses
     */
    public static function obtenerCatalogoMes()
    {
        // Inicializa el arreglo que contiene el catálogo de meses
        $listaMes = array(
            (Object)array('numero' => '01', 'nombre' => 'Enero'),
            (Object)array('numero' => '02', 'nombre' => 'Febrero'),
            (Object)array('numero' => '03', 'nombre' => 'Marzo'),
            (Object)array('numero' => '04', 'nombre' => 'Abril'),
            (Object)array('numero' => '05', 'nombre' => 'Mayo'),
            (Object)array('numero' => '06', 'nombre' => 'Junio'),
            (Object)array('numero' => '07', 'nombre' => 'Julio'),
            (Object)array('numero' => '08', 'nombre' => 'Agosto'),
            (Object)array('numero' => '09', 'nombre' => 'Septiembre'),
            (Object)array('numero' => '10', 'nombre' => 'Octubre'),
            (Object)array('numero' => '11', 'nombre' => 'Noviembre'),
            (Object)array('numero' => '12', 'nombre' => 'Diciembre'),
        );

        return $listaMes;
    }

    /**
     * Función enviarCorreoElectronico
     *
     * Envía un correo electrónico
     *
     * @param array $listaDestinatario Lista de correo electrónico de los destinatarios.
     * @param string $asunto Asunto del correo electrónico.
     * @param string $mensaje Contenido del correo electrónico.
     * @param string $perfil (Opcional) Identificador del perfil para conexión al Servidor de Correo.
     *
     * @throws InputDataException Excepción cuando la cadena no se encuentra dentro de la lista permitida.
     */
    public static function enviarCorreoElectronico($listaDestinatario, $asunto, $mensaje, $perfil = 'default')
    {
        // Genera la instancia para envío de correo electrónico
        $mail = new PHPMailer(true);

        try {
            // Recupera los datos del perfil de conexión al Servidor de Correo
            $datosPerfil = self::obtenerDatosMail($perfil);

            // Configuración para el servidor de correo electrónico
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = $datosPerfil['host'];
            $mail->Port = $datosPerfil['puerto'];
            $mail->Username = $datosPerfil['usuario'];
            $mail->Password = $datosPerfil['contrasena'];
            $mail->CharSet = 'UTF-8';

            // Remitente
            $mail->setFrom($datosPerfil['remitente_correo'], $datosPerfil['remitente_nombre']);

            // Destinatarios
            foreach ($listaDestinatario as $destinatario) {
                $mail->addAddress($destinatario->correo, $destinatario->nombre);
            }

            // Contenido
            $mail->isHTML(true);
            $mail->Subject = $asunto;
            $mail->Body = $mensaje;

            // Envia el correo electrónico
            $mail->send();
        } catch (Exception $e) {
            throw new InputDataException('No fue posible enviar el correo electrónico.<br>Notifique al administrador del sistema.');
        }
    }

    /**
     * Función obtenerTipoUsuario
     *
     * Determina el tipo de usuario recibido.
     *
     * @param string $usuario Nombre de usuario.
     *
     * @return string Identificador del tipo de usuario
     */
    public static function obtenerTipoUsuario($usuario)
    {
        // Realiza las comparaciones necesarias
        if (preg_match(EXPRESION_REGULAR_USUARIO_ALUMNO, $usuario)) {
            return 'alumno';
        } elseif (preg_match(EXPRESION_REGULAR_USUARIO_PROFESOR, $usuario)) {
            return 'profesor';
        } else {
            return 'personalizado';
        }
    }

    /**
     * Función formatearTituloAplicacionTitle
     *
     * Formatea el titulo de una aplicación para mostrarlo en el título de la página.
     *
     * @param string $tituloAplicacion Título de la aplicación.
     *
     * @return string Identificador del tipo de usuario
     */
    public static function formatearTituloAplicacionTitle($tituloAplicacion)
    {
        return str_replace('<br>', ' ', $tituloAplicacion);
    }

    /**
     * Función validarIdCicloEscolar
     *
     * Determina si se recibio un ciclo escolar.
     * Determina si el valor recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarIdCicloEscolar($valor)
    {
        self::validarVacio($valor, 'ciclo escolar');
        if (!preg_match('/^[0-9]{4}$/', $valor)) {
            throw new InputDataException('El Ciclo Escolar no es válido.');
        }
    }

    /**
     * obtenerIPOrigenSolicitud
     *
     * Recupera la dirección IP desde donde se origina la petición
     *
     * @return string Dirección IP de origen
     */
    public static function obtenerIPOrigenSolicitud()
    {
        return current(
            array_filter(
                array(
                    $_SERVER['HTTP_X_REAL_IP'],
                    $_SERVER['HTTP_X_FORWARDED_FOR'],
                    $_SERVER['HTTP_X_FORWARDED'],
                    $_SERVER['HTTP_FORWARDED_FOR'],
                    $_SERVER['HTTP_FORWARDED'],
                    $_SERVER['REMOTE_ADDR']
                )
            )
        );
    }
}

class InputDataException extends Exception
{
    public function __construct($mensaje, $code = 0)
    {
        parent::__construct($mensaje, $code);
    }
}
