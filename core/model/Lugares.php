<?php
require_once 'Conexion_BD.php';
    class Lugares{
        private $db;
        function __construct(){
            $this->db = new Conexion();
        }

        function get_grafica_salon($datos){
            $lugares = array();
            if($datos['salon'][0] == 'null'){
                $query = "select id_semestre, organo, departamento,id_asignatura,substring(salon from 1 for 1) as edificio,asignatura,nombre_docente,nombre tipo_profesor,salon,tipo,cupo,vacantes,(cupo::integer-vacantes::integer) ocupado, hora_inicio,hora_fin,concat(lun,mar,mie,jue,vie,sab) semana from tmp_import where salon is null and id_semestre=:id_semestre";    
                $stmt = $this->db->prepare($query);
            }
            else{
                $query = "select id_semestre, organo, departamento,id_asignatura,substring(salon from 1 for 1) as edificio,asignatura,nombre_docente,nombre tipo_profesor,salon,tipo,cupo,vacantes,(cupo::integer-vacantes::integer) ocupado, hora_inicio,hora_fin,concat(lun,mar,mie,jue,vie,sab) semana from tmp_import where salon=:salon and id_semestre=:id_semestre";
                $stmt = $this->db->prepare($query);
                $stmt->bindValue(":salon",$datos['salon'][0]);
            }
            $stmt->bindValue(":id_semestre",$datos['id_semestre'][0]);
            
            $stmt->execute();
            $lugares=$stmt->fetchAll(PDO::FETCH_CLASS);
            return $lugares;
        }

        function get_grafica_edificio($datos){
            $lugares = array();
            $query = "select id_semestre, organo, departamento,id_asignatura,substring(salon from 1 for 1) as edificio,asignatura,nombre_docente,nombre tipo_profesor,salon,tipo,cupo,vacantes,(cupo::integer-vacantes::integer) ocupado, hora_inicio,hora_fin,concat(lun,mar,mie,jue,vie,sab) semana from tmp_import where substring(salon from 1 for 1)=:edificio and id_semestre=:id_semestre";
            $stmt = $this->db->prepare($query);
            $stmt->bindValue(":id_semestre",$datos['id_semestre'][0]);
            $stmt->bindValue(":edificio",$datos['edificio'][0]);
            $stmt->execute();
            $lugares=$stmt->fetchAll(PDO::FETCH_CLASS);
            return $lugares;
        }
        
        function get_grafica_general($datos){
            $lugares = array();
            $select = "select id_semestre, organo, departamento,id_asignatura,substring(salon from 1 for 1) as edificio,asignatura,nombre_docente,nombre tipo_profesor,salon,tipo,cupo,vacantes,(cupo::integer-vacantes::integer) ocupado, hora_inicio,hora_fin,concat(lun,mar,mie,jue,vie,sab) semana from tmp_import";
            $parametros = array_keys($datos);
            $query = $select;
            foreach ($parametros as $parametro) {
                $query = $query." where";
                $cont=0;
                foreach($datos[$parametro] as $dato){
                    if($parametro == "edificio"){
                        $parametro_conv = "substring(salon from 1 for 1)";
                        $query = $query." $parametro_conv=:$parametro"."_p_$cont"."_p_ or";
                    }else{
                        $query = $query." $parametro=:$parametro"."_p_$cont"."_p_ or";
                    }
                    $cont++;
                }
                $query = rtrim($query,"and");
                $query2 = $query;
                $query = $query." union $select";
            }
            $query = $query2;
            $stmt = $this->db->prepare("select * from ($query) c");
            foreach($parametros as $parametro){
                $cont=0;
                foreach($datos[$parametro] as $dato){
                    $stmt->bindValue(":$parametro"."_p_$cont"."_p_",$dato);
                    $cont++;
                }
            }
            $stmt->execute();
            #return $stmt->errorInfo();
            $lugares=$stmt->fetchAll(PDO::FETCH_CLASS);
            return $lugares;
        }

        function get_edificios(){
            $edificios = array();
            $query = "select distinct substring(salon from 1 for 1) edificio from tmp_import where not (salon ~ '[A-Z][A-Z][A-Z][A-Z]+$') order by edificio asc";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $edificios=$stmt->fetchAll(PDO::FETCH_COLUMN);
            return $edificios;
        }

        function get_semestres(){
            $semestres = array();
            $query = "select distinct id_semestre from tmp_import";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $semestres=$stmt->fetchAll(PDO::FETCH_COLUMN);
            return $semestres;
        }

        function get_salones(){
            $salones = array();
            $query = "select distinct salon from tmp_import order by salon asc";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $salones=$stmt->fetchAll(PDO::FETCH_COLUMN);
            return $salones;
        }

        function get_departamentos(){
            $departamentos = array();
            $query = "select distinct departamento from tmp_import";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $departamentos=$stmt->fetchAll(PDO::FETCH_COLUMN);
            return $departamentos;
        }

        function get_organos(){
            $organos = array();
            $query = "select distinct organo from tmp_import";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $organos=$stmt->fetchAll(PDO::FETCH_COLUMN);
            return $organos;
        }

        function get_tipos_profesor(){
            $tipos_profesor = array();
            $query = "select distinct nombre from tmp_import";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $tipos_profesor=$stmt->fetchAll(PDO::FETCH_COLUMN);
            return $tipos_profesor;
        }

        function get_materias(){
            $materias = array();
            $query = "select distinct id_asignatura, asignatura from tmp_import order by asignatura asc";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $materias=$stmt->fetchAll(PDO::FETCH_CLASS);
            return $materias;
        }

        function get_tipos_asignatura(){
            $tipos_asignatura = array();
            $query = "select distinct tipo from tmp_import order by tipo asc";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $tipos_asignatura=$stmt->fetchAll(PDO::FETCH_COLUMN);
            return $tipos_asignatura;
        }

    }
