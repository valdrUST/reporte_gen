<?php

/**
 * Clase AppHerramientas
 *
 * Métodos y funciones de la aplicación.
 *
 * @package    DASHBOARD
 * @subpackage Aplicacion
 * @category   Herramientas
 * @author     Unidad de Servicios de Cómputo Administrativos
 * @since      Versión 1.0.0
 */
class AppHerramientas extends Herramientas
{
    /**
     * Función validarCodigoPostal
     *
     * Determina si se recibio un código postal.
     * Determina si el código postal recibido cumple con el formato adecuado.
     *
     * @param string $cp Cadena que contiene el código postal.
     * @param string $campo (Opcional) Nombre del campo por validar.
     *
     * @throws InputDataException Excepción cuando el código postal proporcionado no cumple con el formato adecuado.
     */
    public static function validarCodigoPostal($cp, $campo = 'Código Postal')
    {
        self::validarVacio($cp, 'Código Postal');
        if (!preg_match("/^([0-9]{5})$/", $cp)) {
            throw new InputDataException('El campo <strong>' . $campo . '</strong> no cumple con el formato adecuado (5 dígitos).');
        }
    }

    /**
     * Función convertirMayusculas
     *
     * Convierte una cadena a mayúsculas.
     *
     * @param string $cadena Cadena por limpiar.
     *
     * @return string Cadena resultante
     */
    public static function convertirMayusculas($cadena)
    {
        return mb_strtoupper($cadena, 'UTF-8');
    }

    /**
     * Función diferenciaTiempo
     *
     * Obtiene la diferencia de tiempos en segundos
     *
     * @param DateTime $fin
     * @param DateTime $inicio
     * @return string
     */
    public static function diferenciaTiempo($fin, $inicio)
    {
        $diff = $fin->diff($inicio);
        $secs = (((intval($diff->format("%a")) * 24) + intval($diff->format("%H"))) * 60 +
                intval($diff->format("%i"))) * 60 + intval($diff->format("%s"));
        return strval($secs);
    }

    /**
     * Función limpiarAcentos
     *
     * Elimina los caracteres acentuados de una cadena.
     *
     * @param string $cadena Cadena por limpiar.
     * @param boolean $codificarUTF8 Bandera que indica si el resultado debe ser devuelto codificado en UTF-8
     *
     * @return string Cadena resultante
     */
    public static function limpiarAcentos($cadena, $codificarUTF8)
    {
        $originales = 'ÁÉÍÓÚáéíóú';
        $modificadas = 'AEIOUaeiou';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);

        return $codificarUTF8 ? utf8_encode($cadena) : $cadena;
    }

    /**
     * Función convertirMayusculasOld
     *
     * Convierte una cadena a mayúsculas.
     *
     * @param string $cadena Cadena por limpiar.
     * @param boolean $codificarUTF8 (Opcional) Bandera que indica si el resultado debe ser devuelto codificado en UTF-8
     *
     * @return string Cadena resultante
     */
    public static function convertirMayusculasOld($cadena, $codificarUTF8 = true)
    {
        $cadena = str_replace('ñ', 'Ñ', strtoupper(self::limpiarAcentos($cadena, $codificarUTF8)));
        return $cadena;
    }

    /**
     * Función validarAlfanumerico
     *
     * Determina si se recibio una cadena.
     * Determina si la cadena recibida cumple con el formato adecuado.
     *
     * @param string $cadena Cadena por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws     InputDataException   Excepción cuando la cadena no cumple con el formato adecuado.
     */
    public static function validarAlfanumerico($cadena, $campo)
    {
        self::validarVacio($cadena, $campo);
        if (!preg_match("/^([A-Z]|[0-9]|[Ñ]|[ ])+$/", $cadena)) {
            throw new InputDataException("El campo <b>$campo</b> no se reconoce como un formato adecuado.<br>Sólo puede utilizar los siguientes caracteres:<br><ul><li>A - Z (incluyendo Ñ)</li><li>dígitos</li></ul>");
        }
    }

    /**
     * Función validarNombreCompuesto
     *
     * Determina si se recibio un Nombre.
     * Determina si el Nombre recibido cumple con el formato adecuado.
     *
     * @param string $cadena Cadena por validar.
     * @param string $campo Nombre del campo por validar.
     * @param bool $validarCadenaVacia Define si se valida que la cadena no este vacía
     * @throws InputDataException Excepción cuando la cadena no cumple con el formato adecuado.
     */
    public static function validarNombreCompuesto($cadena, $campo, $validarCadenaVacia = true)
    {
        if ($validarCadenaVacia)
            self::validarVacio($cadena, $campo);

        if (strlen($cadena) > 0)
            if (!preg_match("/^([A-Z]|[ÁÉÍÓÚ]|[Ñ]|[ ]|['])+$/", $cadena)) {
                throw new InputDataException("El campo <b>$campo</b> no se reconoce como un formato adecuado.<br>Sólo puede utilizar los siguientes caracteres:<br><ul><li>A - Z (incluyendo Ñ)</li><li>espacio</li><li><b>'</b> (comilla simple)</li></ul>");
            }
    }

    /**
     * Función validarNumeroCuenta
     *
     * Determina si se recibio un número de cuenta.
     * Determina si el número de cuenta recibido cumple con el formato correcto.
     *
     * @param string $cuenta Número de cuenta por validar.
     * @param string $campo (Opcional) Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarNumeroCuenta($cuenta, $campo = 'Número de cuenta')
    {
        self::validarVacio($cuenta, $campo);
        if (!preg_match('/^([0-9]{9})$/', $cuenta)) {
            throw new InputDataException('El campo <strong>' . $campo . '</strong> no cumple con el formato adecuado (9 dígitos).');
        }
    }

    /**
     * Función validarIDTrayectoria
     *
     * Determina si se recibio un Identificador de Trayectoria.
     * Determina si el número de cuenta recibido cumple con el formato correcto.
     *
     * @param string $cuenta Número de cuenta por validar.
     * @param string $campo (Opcional) Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarIDTrayectoria($cuenta, $campo = 'Identificador de trayectoria')
    {
        self::validarVacio($cuenta, $campo);
        if (!preg_match('/^([0-9]{9}[0-9]{8}[0-9]{2}[0-9]{4})$/', $cuenta)) {
            throw new InputDataException('El <strong>' . $campo . '</strong> no cumple con el formato adecuado.');
        }
    }

    /**
     * Función validarClaveAsignatura
     *
     * Determina si se recibio una clave de asignatura.
     * Determina si la clave de asignatura recibida cumple con el formato correcto.
     *
     * @param string $claveAsignatura Clave de la carrera por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarClaveAsignatura($claveAsignatura)
    {
        self::validarVacio($claveAsignatura, 'Clave de asignatura');
        if (!preg_match('/^([0-9]{1,4})$/', $claveAsignatura)) {
            throw new InputDataException('La clave de la asignatura no cumple el formato (1 a 4 dígitos).');
        }
    }

    /**
     * Función validarClaveAsignaturaPosgrado
     *
     * Determina si se recibio una clave de asignatura de posgrado.
     * Determina si la clave de asignatura recibida cumple con el formato correcto.
     *
     * @param string $claveAsignatura Clave de la carrera por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarClaveAsignaturaPosgrado($claveAsignatura)
    {
        self::validarVacio($claveAsignatura, 'Clave de asignatura');
        if (!preg_match('/^([0-9]{4,6})$/', $claveAsignatura)) {
            throw new InputDataException('La clave de la asignatura no cumple el formato (1 a 4 dígitos).');
        }
    }

    /**
     * Función validarClaveCarreraDGAE
     *
     * Determina si se recibio una clave de carrera.
     * Determina si la clave de carrera recibida cumple con el formato correcto para DGAE.
     *
     * @param string $carrera Clave de la carrera por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarClaveCarreraDGAE($carrera, $campo = null)
    {
        $campo .= (is_null($campo)) ? 'Carrera' : 'Carrera ($campo)';
        self::validarVacio($carrera, $campo);
        if (!preg_match('/^([0-9]{3})$/', $carrera)) {
            throw new InputDataException('La carrera seleccionada no es válida.');
        }
    }

    /**
     * Función validarPlanFI
     *
     * Determina si se recibio un Plan.
     * Determina si el Plan recibido cumple con el formato correcto.
     *
     * @param string $plan Clave del plan por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarPlanFI($plan, $campo = null)
    {
        $campo .= (is_null($campo)) ? 'Plan' : "Plan ($campo)";
        self::validarVacio($plan, $campo);
        if (!preg_match('/^((20(06|09|10|16|20))|(19(92|94)))$/', $plan)) {
            throw new InputDataException('El plan selesccionado no es válido.');
        }
    }

    /**
     * Función validarNumeroGrupo
     *
     * Determina si se recibio un número de grupo.
     * Determina si el número de grupo recibido cumple con el formato correcto.
     *
     * @param string $numeroGrupo Número de grupo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarNumeroGrupo($numeroGrupo)
    {
        self::validarVacio($numeroGrupo, 'Número de grupo');
        if (!preg_match('/^([1-9][0-9]|0?[1-9]|10[0-2])$/', $numeroGrupo)) {
            throw new InputDataException('El número de grupo no cumple con el formato (1 a 3 dígitos).');
        }
    }

    /**
     * Función validarNumeroGrupoPosgrado
     *
     * Determina si se recibio un número de grupo de posgrado.
     * Determina si el número de grupo recibido cumple con el formato correcto.
     *
     * @param string $numeroGrupo Número de grupo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarNumeroGrupoPosgrado($numeroGrupo)
    {
        self::validarVacio($numeroGrupo, 'Número de grupo');
        if (!preg_match('/^[A-Z0-9]{1,4}$/', $numeroGrupo)) {
            throw new InputDataException('El número de grupo no cumple con el formato (1 a 4 dígitos/letras).');
        }
    }

    /**
     * Función validarPeriodoExtraordinario
     *
     * Determina si se recibio un periodo.
     * Determina si el periodo recibido cumple con el formato correcto.
     *
     * @param string $periodo Periodo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarPeriodoExtraordinario($periodo)
    {
        self::validarVacio($periodo, 'Periodo');
        if (!preg_match('/^[1-3]{1}$/', $periodo)) {
            throw new InputDataException('El periodo no cumple con el formato (1 a 3).');
        }
    }

    /**
     * Función validarEstatusSolicitudOOPE
     *
     * Determina si se recibio un periodo.
     * Determina si el periodo recibido cumple con el formato correcto.
     *
     * @param string $cadena Estatus por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarEstatusSolicitudOOPE($cadena)
    {
        self::validarVacio($cadena, 'Estatus');
        if (!preg_match('/^[APR]{1}$/', $cadena)) {
            throw new InputDataException('El estatus no cumple con el formato.');
        }
    }

    /**
     * Función validarIdAlumnoDerechoInscripcion
     *
     * Determina si se recibio un identificador.
     * Determina si el identificador recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarIdAlumnoDerechoInscripcion($valor)
    {
        self::validarVacio($valor, 'Identificador de Alumno');
        if (!preg_match('/^([0-9]{14})$/', $valor)) {
            throw new InputDataException('El Identificador del registro no es válido.');
        }
    }

    /**
     * Función validarCupoVacantes
     *
     * Determina si se recibio cupo/vacantes.
     * Determina si el cupo/vacantes recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarCupoVacantes($valor, $campo)
    {
        self::validarVacio($valor, $campo, true);
        if (!preg_match('/^([0-9]{1,2})$/', $valor)) {
            throw new InputDataException('El campo <strong>' . $campo . '</strong> no es válido<br><ul><li>Mínimo: 0</li><li>Máximo: 99</li></ul>');
        }
    }

    /**
     * Función validarEncuestaEnLinea
     *
     * Determina si se recibio el indicador de Encuesta en Linea.
     * Determina si el indicador de Encuesta en Linea recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarEncuestaEnLinea($valor)
    {
        self::validarVacio($valor, 'Encuesta en línea', true);
        if (!preg_match('/^([0-1]{1})$/', $valor)) {
            throw new InputDataException('El campo <strong>Encuesta en línea</strong> no es válido.');
        }
    }

    /**
     * Función validarPrimerIngreso
     *
     * Determina si se recibio el indicador de Primer Ingreso.
     * Determina si el indicador de Primer Ingreso recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarPrimerIngreso($valor)
    {
        self::validarVacio($valor, 'Primer Ingreso', true);
        if (!preg_match('/^([0-1]{1})$/', $valor)) {
            throw new InputDataException('El campo <strong>Primer Ingreso</strong> no es válido.');
        }
    }

    /**
     * Función validarEstatusGrupoAsignatura
     *
     * Determina si se recibio el Estatus del Grupo-Asignatura.
     * Determina si Estatus del Grupo-Asignatura recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarEstatusGrupoAsignatura($valor)
    {
        self::validarVacio($valor, 'Estatus Grupo', true);
        if (!preg_match('/^([0-2]{1})$/', $valor)) {
            throw new InputDataException('El campo <strong>Estatus</strong> no es válido.');
        }
    }

    /**
     * Función validarCurricular
     *
     * Determina si se recibio el indicador de Curricular.
     * Determina si el indicador de Curricular recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarCurricular($valor)
    {
        self::validarVacio($valor, 'Curricular', true);
        if (!preg_match('/^([0-1]{1})$/', $valor)) {
            throw new InputDataException('El campo <strong>Curricular</strong> no es válido.');
        }
    }

    /**
     * Función validarIngles
     *
     * Determina si se recibio el indicador de Inglés.
     * Determina si el indicador de Inglés recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarIngles($valor)
    {
        self::validarVacio($valor, 'Inglés', true);
        if (!preg_match('/^([0-1]{1})$/', $valor)) {
            throw new InputDataException('El campo <strong>Inglés</strong> no es válido.');
        }
    }

    /**
     * Función validarRFC
     *
     * Determina si se recibio un RFC.
     * Determina si el RFC recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarRFC($valor, $campo = 'RFC')
    {
        self::validarVacio($valor, $campo);
        if (!preg_match('/^[A-ZÑ]{4}[0-9]{6}[A-ZÑ0-9]{3}$/', $valor)) {
            throw new InputDataException('El campo <strong>' . $campo . '</strong> no cuenta con un formato válido.');
        }
    }

    /**
     * Función validarCURP
     *
     * Determina si se recibio un CURP.
     * Determina si el CURP recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarCURP($valor)
    {
        self::validarVacio($valor, 'CURP');
        if (!preg_match('/^[A-ZÑ]{4}[0-9]{6}[H|M][A-Z]{2}[A-ZÑ0-9]{3}[0-9]{2}$/', $valor)) {
            throw new InputDataException('El campo <strong>CURP</strong> no cuenta con un formato válido.');
        }
    }

    /**
     * Función validarNumeroTrabajador
     *
     * Determina si se recibio un Número de Trabajador.
     * Determina si el Número de Trabajador recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarNumeroTrabajador($valor)
    {
        self::validarVacio($valor, 'Número de Trabajador');
        if (!preg_match('/^([0-9]{4,7})$/', $valor)) {
            throw new InputDataException('El campo <strong>Número de Trabajador</strong> no cuenta con un formato válido.');
        }
    }

    /**
     * Función validarFolio
     *
     * Determina si se recibio un Número de Trabajador.
     * Determina si el Número de Trabajador recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarFolio($valor)
    {
        self::validarVacio($valor, 'Folio');
        if (!preg_match('/^([0-9]{1,12})$/', $valor)) {
            throw new InputDataException('El campo <strong>Folio</strong> no cuenta con un formato válido.');
        }
    }

    /**
     * Función validarNumeroProfesor
     *
     * Determina si se recibio un Número de Profesor.
     * Determina si el número de profesor recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarNumeroProfesor($valor)
    {
        self::validarVacio($valor, 'Número de profesor');
        if (!preg_match('/^([1-5]{1})$/', $valor)) {
            throw new InputDataException('El campo <strong>Número profesor</strong> no es válido<br><ul><li>Mínimo: 1</li><li>Máximo: 5</li></ul>');
        }
    }

    /**
     * Función validarTipoClase
     *
     * Determina si se recibio un Tipo de Clase.
     * Determina si el tipo de clase recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarTipoClase($valor)
    {
        self::validarVacio($valor, 'Tipo de clase');
        if (!preg_match('/^([1-4]{1})$/', $valor)) {
            throw new InputDataException('El campo <strong>Tipo de clase</strong> no es válido.');
        }
    }

    /**
     * Función validarNombramiento
     *
     * Determina si se recibio un Nombramiento.
     * Determina si el Nombramiento recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarNombramiento($valor)
    {
        self::validarVacio($valor, 'Nombramiento', true);
        if (!preg_match('/^([0-6])$/', $valor)) {
            throw new InputDataException('El campo <strong>Nombramiento</strong> no cuenta con un formato válido.');
        }
    }

    /**
     * Función validarPais
     *
     * Determina si se recibio un País.
     * Determina si el País recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarPais($valor)
    {
        self::validarVacio($valor, 'País', true);
        if (!preg_match('/^([0-9]{1,3})$/', $valor)) {
            throw new InputDataException('El campo <strong>País</strong> no cuenta con un formato válido.');
        }
    }

    /**
     * Función validarEstatusProfesor
     *
     * Determina si se recibio un Estatus.
     * Determina si el Estatus recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarEstatusProfesor($valor)
    {
        self::validarVacio($valor, 'Estatus', true);
        if (!preg_match('/^([0-4]{1})$/', $valor)) {
            throw new InputDataException('El campo <strong>Estatus</strong> no cuenta con un formato válido.');
        }
    }

    /**
     * Función validarTitulo
     *
     * Determina si se recibio un Título.
     * Determina si el Título recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarTitulo($valor)
    {
        self::validarVacio($valor, 'Título', true);
        if (!preg_match('/^([0-9]{1,2})$/', $valor)) {
            throw new InputDataException('El campo <strong>Título</strong> no cuenta con un formato válido.');
        }
    }

    /**
     * Función validarClaveDepartamento
     *
     * Determina si se recibio una clave de departamento.
     * Determina si la clave de departamento recibida cumple con el formato correcto.
     *
     * @param string $departamento Clave del departamento por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarClaveDepartamento($departamento, $campo = null)
    {
        $campo .= (is_null($campo)) ? 'Departamento' : "Departamento ({$campo})";
        self::validarVacio($departamento, $campo);
        if (!preg_match('/^([0-9]{3,4})$/', $departamento)) {
            throw new InputDataException('El departamento seleccionado no es válido.');
        }
    }

    /**
     * Función validarClaveDepartamento
     *
     * Determina si se recibio una clave de departamento.
     * Determina si la clave de departamento recibida cumple con el formato correcto.
     *
     * @param string $division Clave de la División por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarClaveDivision($division, $campo = null)
    {
        $campo .= (is_null($campo)) ? 'División' : 'División ($campo)';
        self::validarVacio($division, $campo);
        if (!preg_match('/^([0-9]{1,2})$/', $division)) {
            throw new InputDataException('La división seleccionada no es válida.');
        }
    }

    /**
     * Función validarFecha
     *
     * Determina si se recibio una Fecha.
     * Determina si la Fecha recibida cumple con el formato correcto.
     * Determina si la Fecha recibida es válida
     *
     * @param string $valor Valor del campo por validar.
     * @param string $campo Nombre del campo por validar.
     * @param string $ordenFecha Orden para la Fecha por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarFecha($valor, $campo = 'Fecha', $ordenFecha = 'AAAA-MM-DD')
    {
        self::validarVacio($valor, $campo);
        switch ($ordenFecha) {
            case 'DD-MM-AAAA':
                $erFecha = '([0-9]{1,2}[-][0-9]{1,2}[-][0-9]{4})';
                $fecha = explode('-', $valor);
                $dia = $fecha[0];
                $mes = $fecha[1];
                $anio = $fecha[2];
                // Sale del condicional
                break;
            case 'DD/MM/AAAA':
                $erFecha = '([0-9]{1,2}[\/][0-9]{1,2}[\/][0-9]{4})';
                $fecha = explode('/', $valor);
                $dia = $fecha[0];
                $mes = $fecha[1];
                $anio = $fecha[2];
                // Sale del condicional
                break;
            case 'AAAA/MM/DD':
                $ordenFecha = 'AAAA/MM/DD';
                $erFecha = '([0-9]{4}[\/][0-9]{1,2}[\/][0-9]{1,2})';
                $fecha = explode('/', $valor);
                $dia = $fecha[2];
                $mes = $fecha[1];
                $anio = $fecha[0];
                // Sale del condicional
                break;
            default:
                $ordenFecha = 'AAAA-MM-DD';
                $erFecha = '([0-9]{4}[-][0-9]{1,2}[-][0-9]{1,2})';
                $fecha = explode('-', $valor);
                $dia = $fecha[2];
                $mes = $fecha[1];
                $anio = $fecha[0];
                // Sale del condicional
                break;
        }

        if (!preg_match('/^' . $erFecha . '$/', $valor)) {
            throw new InputDataException('El campo <strong>' . $campo . '</strong> no es válido, debe respetar el formato <strong>' . $ordenFecha . '</strong>.');
        }
        if (!checkdate($mes, $dia, $anio)) {
            throw new InputDataException('La fecha indicada en el campo <strong>' . $campo . '</strong> no es válida.');
        }
    }

    /**
     * Función validarDiasClase
     *
     * Determina si se recibieron días de clase.
     * Determina si los días de clase recibidos cumplen con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarDiasClase($valor)
    {
        self::validarVacio($valor, 'Días de clase');
        if (!preg_match('/^([0-1]{7})$/', $valor)) {
            throw new InputDataException('El campo <strong>Días de clase</strong> no es válido.');
        } elseif (!preg_match('/(1+)/', $valor)) {
            throw new InputDataException('Es necesario seleccionar al menos un <strong>Día de clase</strong>.');
        }
    }

    /**
     * Función prepararHora
     *
     * Prepara una hora en formato entero.
     *
     * @param string $hora Valor del campo por preparar.
     * @param string $campo Nombre del campo por preparar.
     *
     * @return string Hora generada
     * @throws InputDataException Excepción por datos de entrada.
     *
     */
    public static function prepararHoraBD($hora, $campo)
    {
        // Determina si la hora recibida no sobrepasa el límite de caracteres
        if (strlen($hora) > 4) {
            throw new InputDataException('La hora de <strong>' . $campo . '</strong> no es válida.');
        }

        // En caso de ser necesario, completa la hora recibida
        while (strlen($hora) < 4) {
            $hora = '0' . $hora;
        }

        // Incluye los ":" en la hora recibida
        $hora = substr($hora, 0, 2) . ':' . substr($hora, 2);

        // Valida la hora generada
        self::validarHora($hora, $campo);

        return $hora . ':00';
    }

    /**
     * Función validarHora
     *
     * Determina si se recibio una hora de inicio de clase.
     * Determina si la hora de inicio de clase recibida cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarHora($valor, $campo)
    {
        self::validarVacio($valor, $campo);
        if (!preg_match('/^(0?[0-9]|1[0-9]|2[0-3]):[0-5]{1}[0-9]{1}$/', $valor)) {
            throw new InputDataException('La Hora <strong>' . $campo . '</strong> no es válida, debe cumplir con el formato de 24 horas (HH:MM).');
        }
    }

    /**
     * Función validarHoraInicioFinClase
     *
     * Determina si se recibio una hora de inicio de clase.
     * Determina si la hora de inicio de clase recibida cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarHoraInicioClase($valor)
    {
        self::validarHora($valor, 'Inicio de Clase');
        if (!preg_match('/^(0?6:4[5-9]|5[0-9])|(((0?[7-9])|(1[0-9])|20):[0-5][0-9])|(21:00)$/', $valor)) {
            throw new InputDataException('El campo <strong>Inicio de clase</strong> no es válido, debe cumplir con el formato de 24 horas (HH:MM) dentro del rango <strong>06:45</strong> - <strong>21:00</strong>.');
        }
    }

    /**
     * Función validarHoraFinClase
     *
     * Determina si se recibio una hora de fin de clase.
     * Determina si la hora de fin de clase recibida cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarHoraFinClase($valor)
    {
        self::validarHora($valor, 'Fin de Clase');
        if (!preg_match('/^(0?[8-9]:[0-5][0-9])|((1[0-9]|2[0-1]):[0-5][0-9])|(22:0[0-9]|1[0-5])$/', $valor)) {
            throw new InputDataException('El campo <strong>Fin de clase</strong> no es válido, debe cumplir con el formato de 24 horas (HH:MM) dentro del rango <strong>08:00</strong> - <strong>22:15</strong>.');
        }
    }

    /**
     * Función validarSalon
     *
     * Determina si se recibio un salón.
     * Determina si el salón recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarSalon($valor)
    {
        self::validarVacio($valor, 'Salón');
        if (!preg_match('/^[A-Y][S0-9][0-9]{2}|EXTR$/', $valor)) {
            throw new InputDataException('El campo <strong>Salón</strong> no es válido.');
        }
    }

    /**
     * Función validarIdGrupoAsignatura
     *
     * Determina si se recibio un identificador.
     * Determina si el identificador recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarIdGrupoAsignatura($valor)
    {
        self::validarVacio($valor, 'Identificador de Grupo');
        if (!preg_match('/^([0-9]{5}[0-9]{4}[0-9]{3})$/', $valor)) {
            throw new InputDataException('El Identificador del Grupo no es válido.');
        }
    }

    /**
     * Función validarIdGrupoProfesor
     *
     * Determina si se recibio un identificador.
     * Determina si el identificador recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarIdGrupoProfesor($valor)
    {
        self::validarVacio($valor, 'Identificador de Grupo');
        if (!preg_match('/^([0-9]{5}[0-9]{4}[0-9]{3}[A-Z0-9Ñ]{13}[0-9]{2})$/', $valor)) {
            throw new InputDataException('El Identificador del Grupo-Profesor no es válido.');
        }
    }

    /**
     * Función validarColumnaAux
     *
     * Determina si el valor recibido cumple con el formato correcto.
     *
     * @param string $cadena Valor del campo por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarColumnaAux($cadena, $campo)
    {
        if (!preg_match('/^[A-ZÑÁÉÍÓÚÜ0-9 ]{0,10}$/i', $cadena)) {
            throw new InputDataException('El campo <b>' . $campo . '</b> no se reconoce como un formato adecuado.<br><ul><li>Máximo 10 caracteres</li><li>A - Z (incluyendo Ñ, Á, É, Í, Ó, Ú y Ü)</li><li>dígitos</li></ul>');
        }
    }

    /**
     * Función validarSemestreLectivo
     *
     * Determina si se recibio un semestre.
     * Determina si el valor recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarSemestreLectivo($valor)
    {
        self::validarVacio($valor, 'Semestre lectivo');
        if (!preg_match('/^[0-9]{4}[1-2]{1}$/', $valor)) {
            throw new InputDataException('El Semestre Lectivo no es válido.');
        }
    }

    /**
     * Función esClaveLaboratorio
     *
     * Determina si se recibio una clave de asignatura.
     * Determina si la clave de asignatura recibida es para laboratorio.
     *
     * @param string $claveAsignatura Clave de la asignatura por validar.
     *
     * @return Boolean Bandera que indica si se trata de una clave de Laboratorio
     * @throws InputDataException Excepción por datos de entrada.
     *
     */
    public static function esClaveLaboratorio($claveAsignatura)
    {
        self::validarClaveAsignatura($claveAsignatura);
        $esLaboratorio = ($claveAsignatura > 5000 && $claveAsignatura != '9990') ? true : false;
        return $esLaboratorio;
    }

    /**
     * Función obtenerDatosCURP
     *
     * Obtiene los datos representativos obtenidos de la CURP de una persona.
     *
     * @param string $curp CURP recibida.
     *
     * @return array Lista de datos
     */
    public static function obtenerDatosCURP($curp)
    {
        $listaDatos['sexo'] = (substr($curp, 10, 1) == 'H') ? 'M' : 'F';
        $listaDatos['nacionalidad'] = (substr($curp, 11, 2) == 'NE') ? 'E' : 'M';
        $listaDatos['fecha_nacimiento'] = ((substr($curp, 4, 2) + 2000) > date("Y") ? substr($curp, 4, 2) + 1900 : substr($curp, 4, 2) + 2000) . '-' . substr($curp, 6, 2) . '-' . substr($curp, 8, 2);

        return $listaDatos;
    }

    /**
     * Función validarIndicadorSobrecupo
     *
     * Determina si se recibio el indicador de Sobrecupo.
     * Determina si el indicador de Sobrecupo recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarIndicadorSobrecupo($valor)
    {
        self::validarVacio($valor, 'Sobrecupo', true);
        if (!preg_match('/^([0-1]{1})$/', $valor)) {
            throw new InputDataException('El campo <strong>Sobrecupo</strong> no es válido.');
        }
    }

    /**
     * Función convertirBinarioDias
     *
     * Convierte una cadena binaria que define la semana en un arreglo con nombres de día.
     *
     * @param string $semana Cadena que define la semana en binario.
     *
     * @@return array Arreglo que define la semana en texto.
     */
    public static function convertirBinarioDias($semana)
    {
        // Define los nombres de los días
        $listaDias = array('Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom');

        // Separa la semana en un arreglo
        $semana = str_split($semana);

        // Recorre cada día de la semana recibida
        foreach ($semana as $key => $dia) {
            // Cuando el día no se encuentra activo, elimina el nombre del día
            if ($dia == 0) {
                unset($listaDias[$key]);
            }
        }

        return $listaDias;
    }

    /**
     * Función validarTitulo
     *
     * Determina si se recibio un Título.
     * Determina si el Título recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarLugarAdeudo($valor)
    {
        self::validarVacio($valor, 'Lugar de adeudo');
        if (!preg_match('/^([1-2])$/', $valor)) {
            throw new InputDataException('El campo <strong>Lugar de adeudo</strong> no cuenta con un formato válido.');
        }
    }

    /**
     * Función evitaPeriodoInactivo
     *
     * Usado en la generación de tarjetas para omitir los días de asueto
     *
     * @param $periodosInactivos
     * @param $inicio
     * @return mixed
     * @throws Exception
     */
    public static function evitaPeriodoInactivo($periodosInactivos, $inicio)
    {
        $enPeriodoInactivo = false;
        foreach ($periodosInactivos as $periodo) {
            if ($inicio > DateTime::createFromFormat('Y-m-d', $periodo[0]) && $inicio < DateTime::createFromFormat('Y-m-d', $periodo[1])) {
                do {
                    $inicio->add(new DateInterval('P1D'));
                    $enPeriodoInactivo = $inicio > DateTime::createFromFormat('Y-m-d', $periodo[0]) && $inicio < DateTime::createFromFormat('Y-m-d', $periodo[1]);
                } while ($enPeriodoInactivo);
            }
        }

        return $inicio;
    }

    /**
     * Función empiezaMesEnInactivo
     *
     * Usado para verificar cuando empieza otro mes durante los días de asueto y poder pintar el nombre del mes en la tarjeta
     *
     * @param $periodosInactivos
     * @param $inicio
     * @return bool
     */
    public static function empiezaMesEnInactivo($periodosInactivos, $inicio)
    {
        foreach ($periodosInactivos as $periodo) {
            if ($inicio > DateTime::createFromFormat('Y-m-d', $periodo[0]) && $inicio < DateTime::createFromFormat('Y-m-d', $periodo[1])) {
                if (1 <= intval($inicio->format('j')) && intval($inicio->format('j')) < 7) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Función corregirEncode
     *
     * Método para soportar caracteres especiales que vienen de WEB
     *
     * @param $texto
     * @param string $encode
     * @return string
     */
    public static function corregirEncode($texto, $encode = 'iso-8859-1')
    {
        return iconv(mb_detect_encoding($texto, 'auto'), $encode, $texto);
    }

    /**
     * Función generateRandomString
     *
     * Genera una cadena de caracteres random
     *
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789 abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Función validarIdGrupoAsignaturaExtraordinario
     *
     * Determina si se recibio un identificador.
     * Determina si el identificador recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarIdGrupoAsignaturaExtraordinario($valor)
    {
        self::validarVacio($valor, 'Identificador de Grupo');
        if (!preg_match('/^([0-9]{5}[0-9]{4}[0-9]{2}[0-3])$/', $valor)) {
            throw new InputDataException('El Identificador del Grupo no es válido.');
        }
    }
    
    /**
     * @param string $inicio    Hora de inicio con el formato H:i:s
     * @param string $fin       Hora de término con el formato H:i:s
     * @param string $intervalo Periodo de incremento para generar el intervalo.
     *
     * @return array            Arreglo con los intervalos generados.
     * @throws Exception
     */
    public static function generarIntervaloHoras($inicio, $fin, $intervalo)
    {
        // Inicializa el arreglo contenedor de intervalos.
        $arrayHorasIntervalos = array();

        // Define los objetos de tiempo.
        $tiempoInicio = new DateTime($inicio);
        $tiempoFin = new DateTime($fin);

        // Genera un objeto de intervalo con un periodo a través de un string.
        $intervalo = DateInterval::createFromDateString($intervalo);

        // Realiza la iteración de fechas repitiéndose a intervalos regulares durante el lapso de tiempo indicados y el intervalo.
        $rangos = new DatePeriod($tiempoInicio, $intervalo, $tiempoFin);

        // Construye para cada rango el intervalo correspondiente.
        foreach ($rangos as $hora) {
            $horaIntervalo = $hora->format('H:i') . '-' . $hora->add($intervalo)->format('H:i');
            $arrayHorasIntervalos[$horaIntervalo] = 0;
        }

        return $arrayHorasIntervalos;
    }

    /**
     * redondeaTiempoCuartos function
     *
     * Redondea un periodo de tiempo hacia el intervalo de quince minutos mayor.
     *
     * @param  string $dateTime  Hora que se desea redondear.
     * @param  int    $intervalo Intervalo máximo al qie se debe redondear.
     *
     * @return string            Tiempo redondeado como objeto DateTime
     */
    public static function redondeaTiempoCuartos($dateTime, $intervalo = 15)
    {
        return $dateTime->setTime(
            $dateTime->format('H'),
            ceil($dateTime->format('i') / $intervalo) * $intervalo,
            0
        );
    }

    /**
     * Función validarIDTipoSilla
     *
     * Determina si se recibio el indicador de Tipo de Silla.
     * Determina si el indicador de Tipo de Silla recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarIDTipoSilla($valor, $campo = 'Tipo de Silla')
    {
        self::validarVacio($valor, $campo);
        if (!preg_match('/^([1-2])$/', $valor)) {
            throw new InputDataException('El campo <strong>' . $campo . '</strong> no es válido.');
        }
    }

    /**
     * Función validarIDTipoSalon
     *
     * Determina si se recibio el indicador de Tipo de Salón.
     * Determina si el indicador de Tipo de Salón recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarIDTipoSalon($valor, $campo = 'Tipo de Silla')
    {
        self::validarVacio($valor, $campo);
        if (!preg_match('/^([1-5])$/', $valor)) {
            throw new InputDataException('El campo <strong>' . $campo . '</strong> no es válido.');
        }
    }

    /**
     * Función validarBooleano
     *
     * Determina si se recibio el indicador Booleano.
     * Determina si el indicador Booleano recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     * @param string $campo Nombre del campo por validar.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarBooleano($valor, $campo)
    {
        self::validarVacio($valor, $campo, true);
        if (!preg_match('/^([0-1])$/', $valor)) {
            throw new InputDataException('El campo <strong>' . $campo . '</strong> no es válido.');
        }
    }

    /**
     * Función validarEntero
     *
     * Determina si se recibio un entero.
     * Determina si el entero recibido cumple con el formato correcto.
     *
     * @param string $valor Valor del campo por validar.
     * @param string $campo Nombre del campo por validar.
     * @param string $valorMinimo Valor mínimo que puede alcanzar el campo.
     * @param string $valorMaximo Valor máximo que puede alcanzar el campo.
     * @param Boolean $permitirCero Bandera que indica si el cero es aceptado.
     *
     * @throws InputDataException Excepción por datos de entrada.
     */
    public static function validarEntero($valor, $campo, $valorMinimo, $valorMaximo, $permitirCero)
    {
        self::validarVacio($valor, $campo, $permitirCero);
        if (!preg_match('/^[0-9]+$/', $valor)) {
            throw new InputDataException('El campo <strong>' . $campo . '</strong> no es válido.');
        }

        // Valida el mínimo permitido
        if ($valor < $valorMinimo) {
            throw new InputDataException('El valor mínimo par el campo <strong>' . $campo . '</strong> es <strong>' . $valorMinimo . '</strong>.');
        }

        // Valida el máximo permitido
        if ($valor > $valorMaximo) {
            throw new InputDataException('El valor máximo par el campo <strong>' . $campo . '</strong> es <strong>' . $valorMaximo . '</strong>.');
        }
    }

    /**
     * generaDiasIntervalos function.
     *
     * Genera un arreglo de Días de la Semana con un rango de intervalos dado.
     *
     * @param  string $inicio    Hora de inicio con el formato H:i:s
     * @param  string $fin       Hora de término con el formato H:i:s
     * @param  string $intervalo Periodo de incremento para generar el intervalo.
     *
     * @return array  Arreglo de Días de Semana con Intervalos de Tiempo.
     * @throws Exception
     */
    public static function generaDiasIntervalos($inicio, $fin, $intervalo)
    {
        // Inicializa arreglo de días de la semana.
        $arrayDiasSemana = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');

        // Inicializa el arreglo a devolver.
        $arrayDiasIntervalo = array();

        // Añade a cada día el arreglo de intervalos.
        foreach ($arrayDiasSemana as $dia) {
            $arrayDiasIntervalo[$dia] = self::generarIntervaloHoras($inicio, $fin, $intervalo);
        }

        return$arrayDiasIntervalo;
    }
}
