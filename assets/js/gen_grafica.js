var IC = require('./grafica.js');
console.log("kek");
options_avance_generacion = {
    "series": [
      {
        "name": "0-99%",
        "data": [
          416,
          416,
          416,
          416,
          415,
          415,
          415,
          415,
          369,
          311,
          256,
          225,
          199,
          181,
          178,
          178,
          178,
          178,
          178,
          178
        ]
      },
      {
        "name": "100%",
        "data": [
          0,
          0,
          0,
          0,
          1,
          1,
          1,
          1,
          22,
          40,
          57,
          73,
          83,
          98,
          101,
          101,
          101,
          101,
          101,
          101
        ]
      }
    ],
    "title": {
      "text": "Avance Generacional"
    },
    "legend": {
      "position": "bottom"
    },
    "labels": [],
    "xaxis": {
      "categories": [
        "20131",
        "20132",
        "20141",
        "20142",
        "20151",
        "20152",
        "20161",
        "20162",
        "20171",
        "20172",
        "20181",
        "20182",
        "20191",
        "20192",
        "20201",
        "20202",
        "20211",
        "20212",
        "20221",
        "20222"
      ]
    },
    "chart": {
      "type": "line",
      "animations": {
        "enabled": false
      },
      "toolbar": {
        "show": false
      }
    },
    "plotOptions": {
      "pie": {
        "donut": {
          "labels": {
            "show": true,
            "total": {
              "showAlways": true,
              "show": true
            }
          }
        }
      }
    },
    "dataLabels": {
      "enabled": true,
      "textAnchor": "start"
    }
  };

IC.generateChart(options_avance_generacion,function(svgHtml){
  console.log(svgHtml);
  phantom.exit();
});