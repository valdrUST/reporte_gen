exports.generateChart = function (jsonData, callback) {
    var page = require('webpage').create();
    page.viewportSize = { width: 800, height: 600 };
    page.content = '<html><head><title></title><script src="https://code.jquery.com/jquery-3.4.1.min.js"></script><script src="https://cdn.jsdelivr.net/npm/apexcharts"></script></head><body><div id="chart">Chart did not generate</div></body></html>';
    page.onLoadFinished = function () {
        info = page.evaluate(function (jsonData) {
            var chart_3 = new ApexCharts(document.querySelector("#chart"), jsonData);
            chart_3.render();
            /*var dataURL = chart_3.dataURI().then((uri) => {
                image = uri;
                $.ajax({
                    async: false,
                    url: "../../core/controller/get_image.php",
                    dataType: "json",
                    type: "post",
                    data: {
                        image: uri
                    },
                    success: function (response) {
                        data = response;
                    }
                });
            });*/
        }, jsonData);
        if (typeof callback === 'function') {
            callback(info);
        }
    };
}