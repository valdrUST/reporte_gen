function load_modal(modal = { tags: { div_modal: '.modal-container' }, data: { option_modal: '', title: 'modal' } }, func = function() { return }) {
    $(modal.tags.div_modal).load(modal.data.view_modal, function() {
        $(modal.tags.id_modal).modal(modal.data.option_modal);
        $(modal.tags.title).text(modal.data.title);
        func();
    });
}

function add_options(objetos, id, options) {
    objetos_temp = objetos;
    objetos = [];
    options.forEach(option => {
        objetos_temp.forEach(objeto => {
            html = $.parseHTML(option);
            html[0].id = objeto[id];
            option = html[0].outerHTML;
            if (objeto.options == undefined)
                objeto.options = "";
            objeto.options = objeto.options + (option);
            objetos.push(objeto);
        });
    });
    return objetos;
}

function date_picker_load(divs_id) {
    divs_id.forEach(div_id => {
        $(div_id).datetimepicker({
            format: 'YYYY-MM-DD HH:mm',
            locale: 'es'
        });
        $(div_id).datetimepicker('format', 'YYYY-MM-DD HH:mm');
    });
}

function set_options_date_picker(divs_id) {
    divs_id.forEach(div_id => {
        $(div_id).datetimepicker('format', 'YYYY-MM-DD HH:mm');
    });
}

function load_tabla(id_tabla, tabla, id_objeto, objetos, opciones) {
    add_options(objetos, id_objeto, opciones);
    tabla.data = objetos;
    return $(id_tabla).DataTable(tabla);
}

function disable_input(tag) {
    $(tag).prop('disabled', true);
}

function set_input(tag, value) {
    $(tag).val(value);
}

function parse_strings_to_array_values(object, id_strings) {
    id_strings.forEach(id_string => {
        object[id_string] = parse_string_array_list(object[id_string]);
    });
    return object;
}

function get_id_tag(tag) {
    return $(tag).attr('id');
}

function set_text_tag(tag, text) {
    $(tag).text(text);
}

function select_option(tag) {
    $(tag).attr('selected', 'selected');
}

function get_selected_options(tag) {
    selected_options = [];
    $(tag + ' option').each(function(i) {
        if (this.selected == true) {
            selected_options.push(this.value);
        }
    });
    return selected_options;
}

function swap_class(tag, old_class, new_class) {
    $(tag).removeClass(old_class);
    $(tag).addClass(new_class);
}

function set_active_class(tag) {
    $(".active").removeClass("active");
    $(tag).addClass("active");
}

function add_option_select(tag, tag_id, value, text) {
    $(tag).append("<option id='" + tag_id + "' value='" + value + "'>" + text + "</option>");
}

function parse_string_array_list(string_array) {
    string = string_array.replace('{', '');
    string = string.replace('}', '');
    return string.split(',');
}

function error_server() {
    alert("El servidor esta teniendo problemas con esta peticion");
}

function no_session_server() {
    alert("tu sesion expiro o no tienes permisos para realizar esto");
    sessionStorage.clear();
    location.href = "index.php";
}

function get_value_input(name) {
    return $("input[name='" + name + "']:checked").val();
}

function invalid_login() {
    alert("usuario o contraseña invalidos");
}

function ordenar_z_index(ids) {
    cont = 3000;
    ids.forEach(id => {
        $(id).css("z-index", cont);
        cont--;
    });
}

function get_checklist_selected(tag) {
    return $(tag + ":checked").map(function() {
        return this.id;
    }).get();
}

function get_radio_select(name) {

}
//ajax_request("https://192.168.119.44/ocupacion-de-espacios/src/core/controller/get_data.php","GET",{"grafica":{'edificio':['A','B','C'],'salon':['A101','A102','A103']}})
//ajax_request
function ajax_request(url, type, data, dataType="json", async = false) {
    response = [];
    $.ajax({
        type: type,
        url: url,
        data: data,
        dataType: dataType,
        async: async,
        statusCode: {
            200: function(XHTMLHttpRequest) {
                response = XHTMLHttpRequest;
            },
            401: no_session_server,
            403: invalid_login,
            500: error_server
        }
    });
    return response;
}