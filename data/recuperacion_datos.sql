-- obtener departamentos
select distinct id_departamento::integer, departamento from tmp_import;
-- obtener organo
 select distinct id_organo::integer, organo from tmp_import;
 -- obtener profesores
 select distinct rfc, nombre_docente,nombre from tmp_import;
-- obtener asignaturas
select distinct id_asignatura, asignatura from tmp_import;
-- obtener edificios
select distinct substring(salon from 1 for 1) edificio from tmp_import where not (salon ~ '[A-Z][A-Z][A-Z][A-Z]+$') order by edificio asc;
-- obtener salon por edificio
select distinct substring(salon from 2 for 4) salon from tmp_import where not (salon ~ '[A-Z][A-Z][A-Z][A-Z]+$') and substring(salon from 1 for 1) = 'A';
-- obtener salones disponibles
select distinct salon from tmp_import order by salon;
-- obtener datos de lugar
select id_semestre, organo, departamento,id_asignatura,asignatura,nombre_docente,nombre tipo_profesor,salon,tipo,cupo,vacantes,(cupo::integer-vacantes::integer) ocupado, hora_inicio,hora_fin,concat(lun,mar,mie,jue,vie,sab) from tmp_import;
